export const comp = hd.component`
     var celsius = 100, fahrenheit, kelvin;

     constraint {
       (celsius -> fahrenheit) => celsius * (9/5) + 32;
       (fahrenheit -> celsius) => (fahrenheit - 32) * (5/9);
     }
     constraint {
          (celsius -> kelvin) => 273.15 + celsius;
          (kelvin -> celsius) => kelvin - 273.15;
      }
`;
