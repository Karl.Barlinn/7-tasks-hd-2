import { numberBinder } from "../packages/binders.js";
import { comp } from "./comp.hd.js";

let system = hd.defaultConstraintSystem;

window.onload = () => {
  system.addComponent(comp);
  system.update();

  numberBinder(document.getElementById("celsius"), comp.vs.celsius);
  numberBinder(document.getElementById("fahrenheit"), comp.vs.fahrenheit);
  numberBinder(document.getElementById("kelvin"), comp.vs.kelvin);
};
