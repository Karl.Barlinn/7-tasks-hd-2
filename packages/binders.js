export function onHDChange(hdVarRef, func) {
  // console.assert(element != null, "Element is null");
  console.assert(hdVarRef != null, "Hd variable is null");
  console.assert(
    typeof func == "function",
    `func is not function but '${typeof func}'`
  );

  hd.retainingVariableListener(hdVarRef, func);
  // hdVarRef.value.subscribe({
  //   next: (val) => {
  //     if (val.hasOwnProperty("value")) {
  //       func(val.value);
  //     }
  //   },
  // });
}

export function hdListener(element, value, type) {
  console.assert(element != null, "Element is null");
  console.assert(value != null, "Hd variable is null");
  console.assert(
    typeof type == "string",
    `Type is not string but '${typeof type}'`
  );
  value.value.subscribe({
    next: (val) => {
      if (val.hasOwnProperty("value")) {
        element[type] = val.value;
      }
    },
  });
}

export function binder(element, value, attribute, event = "change") {
  hd.elementAttributeBinder(value, element, attribute, event, -1);
}

export function valueBinder(element, value) {
  binder(element, value, "value");
}

export function numberBinder(element, value) {
  binder(element, value, "valueAsNumber");
}

export function disabledBinder(element, value) {
  binder(element, value, "disabled");
}

export function maxBinder(element, value) {
  binder(element, value, "max");
}

export function checkedBinder(element, value) {
  binder(element, value, "checked");
}

export function enabledListener(element, value) {
  // value.value.subscribe({
  //   next: (val) => {
  //     if (val.hasOwnProperty("value")) {
  //       element["disabled"] = !val.value;
  //     }
  //   },
  // });

  hd.variableListener(value, (v) => {
    element.disabled = !v;
  });
}

export function innerTextBinder(text, value) {
  binder(text, value, "innerText");
  // value.value.subscribe({
  //   next: (val) => {
  //     if (val.hasOwnProperty("value")) {
  //       text.innerText = value.value.value;
  //     }
  //   },
  // });
}
