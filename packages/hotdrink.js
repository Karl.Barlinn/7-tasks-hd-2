!(function (e, t) {
  "object" == typeof exports && "object" == typeof module
    ? (module.exports = t())
    : "function" == typeof define && define.amd
    ? define("hd", [], t)
    : "object" == typeof exports
    ? (exports.hd = t())
    : (e.hd = t());
})(self, function () {
  return (() => {
    "use strict";
    var __webpack_modules__ = {
        84: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.retainingVariableListener = function (
              e,
              t,
              n = () => {},
              r = () => {},
              o = () => {}
            ) {
              let s;
              const a = () => {
                s.unsubscribe();
                const i = e.value;
                null != i && (s = u(i, t, n, r, a)), o(null != i);
              };
              return (
                (s = u(e, t, n, r, a)),
                new i.SimpleSubscription(() => s.unsubscribe())
              );
            }),
            (t.variableListener = u);
          var r = a(n(927)),
            o = a(n(831)),
            s = n(775),
            i = n(534);
          function a(e) {
            return e && e.__esModule ? e : { default: e };
          }
          function u(e, t, n = () => {}, i = (e) => {}, a = () => {}) {
            let u;
            if (e instanceof o.default) {
              if (null == e.value)
                throw new Error("Cannot subscribe to a danging variable");
              s.hdconsole.assert(
                e.value instanceof r.default,
                "VariableReference does not hold an instance of Variable"
              ),
                (u = e.value);
            } else u = e;
            if (null == u) throw new Error("variable is null");
            const l = {
              next: (e) => {
                let r = "???";
                try {
                  void 0 !== e.error || !0 === e.inError
                    ? ((r = "onError"), i(e.error))
                    : !0 === e.pending
                    ? ((r = "onPending"), n())
                    : void 0 !== e.value && ((r = "onChange"), t(e.value));
                } catch (t) {
                  s.hdconsole.warn(
                    `Variable listener threw exception in ${r} (given event ${JSON.stringify(
                      e
                    )}):`,
                    t
                  );
                }
              },
              error: i,
              complete: a,
            };
            return u.subscribe(l);
          }
        },
        673: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.bidirectionalElementListener = c),
            (t.elementAttributeBinder = function (
              e,
              t,
              n = "value",
              r = "change",
              o = 0,
              s = (...e) => {},
              i = (...e) => {}
            ) {
              if (!(n in t))
                throw new Error(
                  `Failed to find attribute '${n}' in given element`
                );
              return c(
                e,
                t,
                r,
                o,
                (e, t, r) => {
                  s(null != e ? e : null, t, r);
                  try {
                    r[n] = e;
                  } catch (t) {
                    u.hdconsole.error(
                      `Failed to set attribute '${n}' in element with id '${r.id}' to '${e}'`,
                      t
                    );
                  }
                },
                (e, t, r) => {
                  var o;
                  if (
                    "checkValidity" in r &&
                    "function" == typeof r.checkValidity &&
                    !1 === r.checkValidity()
                  )
                    return;
                  const s = null !== (o = r[n]) && void 0 !== o ? o : null;
                  i(s, t, r), t.set(s);
                }
              );
            });
          var r = l(n(927)),
            o = l(n(831)),
            s = n(534),
            i = n(84),
            a = n(292),
            u = n(775);
          function l(e) {
            return e && e.__esModule ? e : { default: e };
          }
          function c(e, t, n = "change", u = 0, l, c) {
            let h, d;
            if (e instanceof o.default) h = e;
            else {
              if (!(e instanceof r.default))
                throw new Error("Not given a variable or VariableReference");
              h = e.owner;
            }
            if (u > 0) {
              let e;
              d = (n) => {
                clearTimeout(e),
                  (e = setTimeout(
                    () => c(n, (0, a.variableFromReference)(h), t),
                    u
                  ));
              };
            } else d = (e) => c(e, (0, a.variableFromReference)(h), t);
            t.addEventListener(n, d);
            let f = (0, i.retainingVariableListener)(h, (e) =>
              l(e, (0, a.variableFromReference)(h), t)
            );
            return (
              f.additionalSubscriptions.push(
                new s.SimpleSubscription(() => t.removeEventListener(n, d))
              ),
              f
            );
          }
        },
        329: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.VariableActivation =
              t.TrackingPromise =
              t.MethodActivation =
              t.EvaluationGraph =
              t.Deferred =
                void 0),
            (r = n(927)) && r.__esModule;
          var r,
            o = n(292),
            s = n(268),
            i = n(775);
          class a {
            constructor() {
              this._promise = new Promise((e, t) => {
                (this.resolve = (t) => {
                  e(t);
                }),
                  (this.reject = (e) => {
                    t(e);
                  });
              });
            }
            get promise() {
              return this._promise;
            }
          }
          t.Deferred = a;
          class u {
            constructor(e, t) {
              (this._promise = e), (this._fulfillTracker = t);
            }
            then(e, t) {
              return (
                null != e && this._fulfillTracker(), this._promise.then(e, t)
              );
            }
            catch(e) {
              return this._promise.catch(e);
            }
            finally(e) {
              return this._promise.finally(e);
            }
          }
          t.TrackingPromise = u;
          class l {
            constructor(e) {
              (this._pending = !1),
                (this._variable = e),
                (this._deferred = new a()),
                (this._outMethods = new Set()),
                (this._inMethod = null),
                (0, s.mkRefCounted)(this, (e) => {
                  e.reject(l.abandoned), (e._inMethod = null);
                });
            }
            get name() {
              return this._variable.name;
            }
            get inMethod() {
              return this._inMethod;
            }
            get outMethods() {
              return this._outMethods;
            }
            outMethodsSize() {
              return this._outMethods.size;
            }
            addOutMethod(e) {
              this._outMethods.add(e);
            }
            setInMethod(e) {
              this._inMethod = e;
            }
            resolve(e) {
              return (this._pending = !1), this._deferred.resolve(e);
            }
            reject(e) {
              return (this._pending = !1), this._deferred.reject(e);
            }
            get promise() {
              return this._deferred.promise;
            }
          }
          (t.VariableActivation = l),
            (l.abandoned = {}),
            (t.MethodActivation = class {
              constructor(e, t) {
                (this._f = e), (this._mask = t);
              }
              get ins() {
                return this._ins;
              }
              get outs() {
                return this._outs;
              }
              insSize() {
                return this._ins.length;
              }
              outsSize() {
                return this._outs.length;
              }
              async execute(e, t) {
                (this._ins = e),
                  (this._outs = t),
                  t.forEach((e) => e.setInMethod(this)),
                  e.forEach((e) => (0, s.retain)(e));
                let n = t.map((e) =>
                  e.promise.then(
                    (e) => null,
                    (e) => null
                  )
                );
                Promise.all(n).then(() => {
                  e.forEach((e) => {
                    (0, s.release)(e);
                  });
                });
                let r,
                  l = e.filter((e, t) => 0 == (this._mask[t] & o.maskPromise)),
                  c = e.filter((e, t) => 0 != (this._mask[t] & o.maskUpdate)),
                  h = c.length;
                if (h > 0) {
                  let e = new a(),
                    t = [];
                  for (let n = 0; n < c.length; ++n)
                    t.push(c[n].name),
                      (0, s.setUniqueHandler)(c[n], (t) => {
                        0 == --h && e.resolve(null);
                      });
                  await e.promise;
                }
                try {
                  r = await Promise.all(l.map((e) => e.promise));
                } catch (e) {
                  return void t.forEach((e) =>
                    e.reject("a method's input was rejected")
                  );
                }
                let d,
                  f = [],
                  p = 0;
                for (let t = 0; t < e.length; ++t)
                  0 != (this._mask[t] & o.maskPromise)
                    ? (f[t] = new u(e[t].promise, () =>
                        e[t].addOutMethod(this)
                      ))
                    : (e[t].addOutMethod(this), (f[t] = r[p++]));
                try {
                  d = await this._f(...f);
                } catch (e) {
                  return void t.forEach((t) => t.reject(e));
                }
                if (t.length > 1) {
                  i.hdconsole.assert(
                    Array.isArray(d) && t.length === d.length,
                    `Method result should be an array of ${t.length} elements`
                  );
                  for (let e = 0; e < d.length; ++e) t[e].resolve(d[e]);
                } else t[0].resolve(d);
              }
            }),
            (t.EvaluationGraph = class {
              adjacentOutVertices(e) {
                return e instanceof l ? e.outMethods : e.outs;
              }
              outDegree(e) {
                return e instanceof l ? e.outMethodsSize() : e.outsSize();
              }
              adjacentInVertices(e) {
                return e instanceof l
                  ? null != e.inMethod
                    ? [e.inMethod]
                    : []
                  : e.ins;
              }
              inDegree(e) {
                return e instanceof l
                  ? null != e.inMethod
                    ? 1
                    : 0
                  : e.insSize();
              }
            });
        },
        230: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.LEFT = t.ComponentScope = void 0),
            Object.defineProperty(t, "ParseState", {
              enumerable: !0,
              get: function () {
                return r.ParseState;
              },
            }),
            (t.TypeError = t.SetScope = t.RIGHT = void 0),
            (t.asLeft = f),
            (t.asRight = p),
            (t.block = void 0),
            (t.buildComponent = L),
            (t.checkComponent = k),
            (t.checkConstraint = C),
            (t.checkFirstMethod = w),
            (t.checkOtherMethod = S),
            (t.checkParameterList = g),
            (t.checkQualifiers = y),
            (t.component = function (e, ...t) {
              let n = (0, r.hdl)(e, ...t),
                o = (0, r.must)(
                  (0, r.pBind_)(
                    r.manyws,
                    (0, r.oneOf)(z, B((0, a.freshComponentName)()))
                  )
                )(n);
              return (0, r.must)(r.eof)(n), L(p(k(o.value)));
            }),
            (t.functionBody = void 0),
            Object.defineProperty(t, "hdl", {
              enumerable: !0,
              get: function () {
                return r.hdl;
              },
            }),
            (t.isLeft = d),
            (t.isRight = function (e) {
              return 1 === e.tag;
            }),
            (t.jsCode = M),
            (t.left = c),
            Object.defineProperty(t, "mkParseState", {
              enumerable: !0,
              get: function () {
                return r.mkParseState;
              },
            }),
            (t.pComponent = z),
            (t.pConstraint = D),
            (t.pMethod = A),
            (t.pVariables = V),
            (t.qualifiedParameterList = T),
            (t.qualifierList = E),
            (t.right = h),
            (t.stringLiteral = O),
            (t.templateLiteral = x),
            (t.topComponent = B),
            (t.withPos = m),
            l(n(779));
          var r = n(316),
            o = (n(877), l(n(894))),
            s = l(n(916)),
            i = l(n(137)),
            a = (l(n(831)), n(292)),
            u = n(268);
          function l(e) {
            return e && e.__esModule ? e : { default: e };
          }
          function c(e) {
            return { tag: 0, value: e };
          }
          function h(e) {
            return { tag: 1, value: e };
          }
          function d(e) {
            return 0 === e.tag;
          }
          function f(e) {
            if (0 !== e.tag) throw "Either cast error: no value on left";
            return e.value;
          }
          function p(e) {
            if (1 !== e.tag) throw e.value;
            return e.value;
          }
          function m(e) {
            return (0, r.lift2)((e, t) => ({ pos: e, value: t }))(
              r.currentPos,
              e
            );
          }
          (t.LEFT = 0), (t.RIGHT = 1);
          class _ {
            constructor(e) {
              this._component = e;
            }
            get(e) {
              let t = this._component;
              for (; null != t; ) {
                let n = t.variableDeclarations.get(e);
                if (void 0 !== n) return n;
                t = t.parent;
              }
            }
            has(e) {
              return void 0 !== this.get(e);
            }
          }
          t.ComponentScope = _;
          class v {
            constructor(e) {
              this._set = e;
            }
            get(e) {
              return this.has(e) ? e : void 0;
            }
            has(e) {
              return this._set.has(e);
            }
          }
          t.SetScope = v;
          class b extends Error {
            constructor(e, t) {
              super(), (this._msg = e), (this._pos = t);
            }
            get message() {
              return this._pos.toString() + "\n" + this._msg;
            }
            set message(e) {
              throw Error("Cannot set the name of ParseError");
            }
          }
          function y(e) {
            if (e.length > 1) {
              let t = new Set();
              for (let n of e) {
                if (t.has(n.value))
                  return c(
                    new b(`The same qualifier ${n.value} appears twice`, n.pos)
                  );
                t.add(n.value);
              }
            }
            return h(null);
          }
          function g(e, t) {
            let n = new Set();
            for (let r of e) {
              let e = y(r.value.qualifiers);
              if (d(e)) return c(f(e));
              let o = r.value.parameterName;
              if (!t.has(o.value))
                return c(new b(`Undefined variable name ${o.value}`, o.pos));
              if (n.has(o.value))
                return c(
                  new b(
                    `Parameter ${o.value} appears twice in the same parameter list`,
                    o.pos
                  )
                );
              n.add(o.value);
            }
            return h(n);
          }
          function w(e, t) {
            let n = g(e.ins, t);
            if (d(n)) return n;
            let r = g(e.outs, t);
            return d(r) ? r : h((0, u.setUnion)(p(n), p(r)));
          }
          function S(e, t) {
            let n = new v(t),
              r = g(e.ins, n);
            if (d(r)) return c(f(r));
            let o = g(e.outs, n);
            if (d(o)) return c(f(o));
            let s = (0, u.setUnion)(p(r), p(o));
            if (!(0, u.setEquals)(s, t)) {
              let n = Array.from((0, u.setDifference)(t, s)).join();
              return c(
                new b(
                  "All methods in the same constraint must use the same variables; this method does not refer to variable " +
                    n,
                  e.pos
                )
              );
            }
            return h(null);
          }
          function C(e, t) {
            if (0 === e.methods.length)
              return (e.variableNames = new Set()), h(e);
            let n = w(e.methods[0], t);
            if (d(n)) return c(f(n));
            e.variableNames = p(n);
            for (let t = 1; t < e.methods.length; ++t) {
              let r = S(e.methods[t], p(n));
              if (d(r)) return c(f(r));
            }
            return h(e);
          }
          function k(e) {
            for (let t of e.constraints) {
              let n = C(t, new _(e));
              if (d(n)) return c(f(n));
            }
            for (let t of e.components) {
              let e = k(t);
              if (d(e)) return c(f(e));
            }
            return h(e);
          }
          function M(e, t = !0) {
            return (n) => {
              let o = "";
              for (;;) {
                let s = n.peek();
                if (null == s)
                  return e.includes("")
                    ? (0, r.ok)(o, o.length > 0)
                    : (0, r.failF)(() => ["<js code>"].concat(e), o.length > 0);
                if (e.includes(s))
                  return t && ((0, r.item)(n), (o += s)), (0, r.ok)(o);
                if (["}", "]", ")"].includes(s))
                  return (0, r.fail)(["not " + s], !0);
                let i = (0, r.oneOf)(
                  O,
                  x,
                  (0, r.pBind)((0, r.sat)(R), (e) =>
                    (0, r.pBind)(M([P(e)], !0), (t) => (0, r.ret)(e + t))
                  ),
                  r.item
                )(n);
                if (!i.success) return i;
                o += i.value;
              }
              throw null;
            };
          }
          function R(e) {
            return "(" === e || "{" === e || "[" === e;
          }
          function P(e) {
            switch (e) {
              case "(":
                return ")";
              case "{":
                return "}";
              case "[":
                return "]";
            }
            throw Error(
              "HotDrink internal error: no closing brace defined for " + e
            );
          }
          function O(e) {
            let t = (0, r.oneOf)((0, r.pChar)("'"), (0, r.pChar)('"'))(e);
            if (!t.success) return t;
            const n = t.value,
              o = "\\" + n;
            let s = t.value;
            return (
              (t = (0, r.lift)(
                (e, t) => s + e.join("") + t,
                (0, r.many)(
                  (0, r.oneOf)(
                    (0, r.exactString)("\\\\"),
                    (0, r.exactString)(o),
                    (0, r.sat)((e) => e !== n)
                  )
                ),
                (0, r.pChar)(n)
              )(e)),
              (t.consumed = !0),
              t
            );
          }
          function x(e) {
            const t = "`";
            let n = (0, r.pChar)(t)(e);
            if (!n.success) return n;
            let o = n.value;
            for (;;) {
              let n = (0, r.must)(
                (0, r.oneOf)(
                  (0, r.pChar)(t),
                  (0, r.exactString)("${"),
                  (0, r.exactString)("\\\\"),
                  (0, r.exactString)("\\`"),
                  r.item
                )
              )(e);
              if (n.value === t) {
                o += t;
                break;
              }
              "${" !== n.value
                ? (o += n.value)
                : ((o += "${"), (o += (0, r.must)(M(["}"]))(e).value));
            }
            return (0, r.ok)(o);
          }
          t.TypeError = b;
          const N = (0, r.context)(
            "<function body block>",
            (0, r.token)(
              (0, r.lift)((e, t) => e + t, (0, r.pChar)("{"), M(["}"]))
            )
          );
          t.block = N;
          const j = (0, r.context)(
            "<function body>",
            (0, r.oneOf)(
              N,
              (0, r.pBind)((0, r.token)(M([";"])), (e) =>
                (0, r.ret)("return " + e)
              )
            )
          );
          function A(e) {
            const t = m(
              (0, r.oneOf)(
                (0, r.lift)((e, t) => e, r.pSplice, (0, r.word)(";")),
                (0, r.pBind_)((0, r.word)("=>"), j),
                N
              )
            );
            return (0, r.context)(
              "<method>",
              (0, r.lift)(
                function (e, t, n, r, o) {
                  let s = n.map((e) =>
                    (0, u.foldl)(
                      (e, t) =>
                        (function (e) {
                          switch (e) {
                            case "*":
                              return a.maskPromise;
                            case "!":
                              return a.maskUpdate;
                            default:
                              return a.maskNone;
                          }
                        })(e) | t,
                      a.maskNone,
                      e.value.qualifiers.map((e) => e.value)
                    )
                  );
                  const i = n.map((e) => e.value.parameterName.value);
                  let l;
                  return (
                    (l =
                      "string" == typeof o.value
                        ? { pos: o.pos, value: new Function(i.join(), o.value) }
                        : o),
                    {
                      pos: e,
                      name: t,
                      ins: n,
                      outs: r,
                      code: l,
                      promiseMask: s,
                    }
                  );
                },
                r.currentPos,
                (0, r.opt)(m(r.identifier), null),
                (0, r.ignore)((0, r.word)("(")),
                T("*!"),
                (0, r.ignore)((0, r.word)("->")),
                T(""),
                (0, r.ignore)((0, r.word)(")")),
                t
              )
            )(e);
          }
          function E(e) {
            return (0, r.many)((0, r.token)(m((0, r.pChars)(e))));
          }
          function T(e = "*!") {
            return (0, r.sepList)(
              m(
                (0, r.lift)(
                  (e, t) => ({ qualifiers: e, parameterName: t }),
                  E(e),
                  m(r.identifier)
                )
              ),
              (0, r.word)(",")
            );
          }
          function V(e) {
            return (0, r.inBetween)(
              (0, r.sepList)($, (0, r.word)(",")),
              (0, r.word)("var"),
              (0, r.word)(";")
            )(e);
          }
          function $(e) {
            return (0, r.pBind)(r.currentPos, (e) =>
              (0, r.oneOf)(
                (0, r.lift)(
                  (t) => ({ pos: e, name: t, isReference: !0 }),
                  (0, r.ignore)((0, r.word)("&")),
                  r.identifier
                ),
                (0, r.lift)(
                  (t, n) => ({
                    pos: e,
                    name: t,
                    isReference: !1,
                    initializer: n,
                  }),
                  r.identifier,
                  (0, r.opt)(m(I), void 0)
                )
              )
            )(e);
          }
          function I(e) {
            return (0, r.pBind_)(
              (0, r.word)("="),
              (0, r.context)(
                "<intitializer-expression>",
                (0, r.pBind)(M([",", ";"], !1), (e) =>
                  (0, r.ret)(new Function("return " + e)())
                )
              )
            )(e);
          }
          function D(e) {
            return (0, r.context)(
              "<constraint>",
              (0, r.lift)(
                (e, t, n) => ({
                  pos: e,
                  name: t,
                  variableNames: null,
                  methods: n,
                }),
                r.currentPos,
                (0, r.ignore)((0, r.word)("constraint")),
                (0, r.opt)(m(r.identifier), null),
                (0, r.ignore)((0, r.word)("{")),
                (0, r.many)(A),
                (0, r.ignore)((0, r.word)("}"))
              )
            )(e);
          }
          t.functionBody = j;
          const q = (e, t, n) => {
              let r = new Map(),
                o = [],
                s = [],
                i = {
                  parent: null,
                  pos: e,
                  name: t,
                  variableDeclarations: r,
                  constraints: o,
                  components: s,
                };
              for (let e of n)
                switch (e.tag) {
                  case "variable":
                    for (let t of e.value) r.set(t.name, t);
                    break;
                  case "constraint":
                    o.push(e.value);
                    break;
                  case "component":
                    (e.value.parent = i), s.push(e.value);
                }
              return i;
            },
            F = (0, r.many)(
              (0, r.oneOf)(
                (0, r.pBind)(V, (e) =>
                  (0, r.ret)({ tag: "variable", value: e })
                ),
                (0, r.pBind)(D, (e) =>
                  (0, r.ret)({ tag: "constraint", value: e })
                ),
                (0, r.pBind)(z, (e) =>
                  (0, r.ret)({ tag: "component", value: e })
                )
              )
            );
          function B(e) {
            return (t) => (0, r.lift)(q, r.currentPos, m((0, r.ret)(e)), F)(t);
          }
          function z(e) {
            return (0, r.context)(
              "<component>",
              (0, r.lift)(
                q,
                r.currentPos,
                (0, r.ignore)((0, r.word)("component")),
                m(r.identifier),
                (0, r.ignore)((0, r.word)("{")),
                F,
                (0, r.ignore)((0, r.word)("}"))
              )
            )(e);
          }
          function L(e, t = null) {
            let n = new o.default(e.name.value);
            null != t && t.addComponent(n);
            for (let t of e.variableDeclarations.values())
              if (t.isReference) n.emplaceVariableReference(t.name);
              else {
                let e = null != t.initializer ? t.initializer.value : void 0;
                n.emplaceVariable(t.name, e);
              }
            return (
              e.constraints.forEach((e) =>
                (function (e, t) {
                  let n = t.variableNames;
                  if (null == n)
                    throw Error(
                      "Constraint node must be type checked before it is used for building a constraint"
                    );
                  let r = new Map(),
                    o = 0;
                  for (let e of n) r.set(e, o++);
                  let a = t.methods.map((e) => {
                      return (
                        (t = e),
                        (n = r),
                        new i.default(
                          n.size,
                          t.ins.map((e) => n.get(e.value.parameterName.value)),
                          t.outs.map((e) => n.get(e.value.parameterName.value)),
                          t.promiseMask,
                          t.code.value,
                          (null === (o = t.name) || void 0 === o
                            ? void 0
                            : o.value) || ""
                        )
                      );
                      var t, n, o;
                    }),
                    u = Array.from(r.keys(), (t) => e.getVariableReference(t)),
                    l = new s.default(a),
                    c = null != t.name ? t.name.value : null;
                  return e.emplaceConstraint(c, l, u, !1);
                })(n, e)
              ),
              e.components.forEach((e) => L(e, n)),
              n
            );
          }
        },
        894: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.default = void 0);
          var r = n(775),
            o = (f(n(916)), f(n(98)), f(n(831))),
            s = f(n(927)),
            i = f(n(779)),
            a = n(292),
            u = n(121),
            l = n(269),
            c = n(814),
            h = n(794),
            d = n(501);
          function f(e) {
            return e && e.__esModule ? e : { default: e };
          }
          class p {
            constructor(e) {
              (this._owner = null),
                (this._system = null),
                (this._name = e),
                (this._vars = new Set()),
                (this._varRefs = new Map()),
                (this._varRefNames = new Map()),
                (this._constraints = new Map()),
                (this._constraintNames = new Map()),
                (this._nextFreshCIndex = 0),
                (this._components = new Map());
            }
            get edit() {
              const e = this;
              function t(t) {
                const o = r(t) ? t.substring(0, t.length - n.length) : t,
                  s = e.vs[o];
                if (null == s)
                  throw `Unknown variable '${o}' in component '${
                    e.name
                  }'. Known variables: ${Array.from(e._varRefs.keys()).join(
                    ", "
                  )}`;
                return s;
              }
              const n = "_self";
              function r(e) {
                return e.endsWith(n);
              }
              return new Proxy(
                {},
                {
                  get(e, n, o) {
                    var s;
                    if ("symbol" == typeof n)
                      throw "Dont know how to handle symbols, only strings";
                    const i = t(n);
                    return r(n)
                      ? i
                      : null === (s = i.value) || void 0 === s
                      ? void 0
                      : s.value;
                  },
                  set(n, r, o, i) {
                    if ("symbol" == typeof r)
                      throw "Dont know how to handle symbols, only strings";
                    const u = t(r).value;
                    if (null != u) u.set(o);
                    else {
                      if (!((0, a.isVariable)(o) || o instanceof s.default))
                        throw "Cannot set a dangling variable reference to the given value. It must be a Variable";
                      e.vs[r] = o;
                    }
                    return !0;
                  },
                  ownKeys: function (t, n) {
                    return [...e._varRefs.keys()];
                  },
                  has: function (t, n) {
                    return n in e._varRefs.keys();
                  },
                  isExtensible: () => !1,
                }
              );
            }
            clone(e = (0, a.freshComponentName)()) {
              let t = new p(e),
                n = new Map();
              for (let [e, r] of this._varRefs)
                r.isOwningReference()
                  ? n.set(r, t.emplaceVariable(e))
                  : n.set(r, t.emplaceVariableReference(e));
              for (let [e, r] of this._constraints) {
                let o = r._varRefs.map((e) => n.get(e));
                (0, a.isStay)(r) ||
                  t.emplaceConstraint(e, r._cspec, o, r._optional);
              }
              for (let [e, n] of this._components) t.addComponent(n.clone(e));
              return t;
            }
            get system() {
              return this._system;
            }
            get name() {
              return this._name;
            }
            get owner() {
              return this._owner;
            }
            connectSystem(e) {
              return null != this._owner
                ? (r.hdconsole.error(
                    `Tried to connect the nested component ${this._name}`
                  ),
                  !1)
                : this._system === e
                ? (r.hdconsole.warn(
                    `Same system connected twice to component ${this._name}`
                  ),
                  !1)
                : null != this._system
                ? (r.hdconsole.error(
                    `Trying to connect an already connected component ${this._name} to a another system`
                  ),
                  !1)
                : e._componentNames.has(this.name)
                ? (r.hdconsole.error(
                    `There already exists a component with the name ${this._name} in the system`
                  ),
                  !1)
                : (e._componentNames.add(this.name),
                  e.recorder.record(new c.ComponentAddAction(e, this)),
                  this._connectSystem(e),
                  !0);
            }
            _connectSystem(e) {
              this._system = e;
              for (let t of this._components.values()) t._connectSystem(e);
              for (let t of this._constraints.values())
                0 === t._danglingCount && e.addConstraint(t);
            }
            disconnectSystem() {
              let e = this._system;
              return null == e
                ? (r.hdconsole.error(
                    `Tried to disconnect a component ${this._name} that is already disconnected`
                  ),
                  !1)
                : null != this._owner
                ? (r.hdconsole.error(
                    `Tried to disconnect a subcomponent ${this._name} when owner is still connected`
                  ),
                  !1)
                : (e._componentNames.delete(this.name),
                  e.recorder.record(new d.ComponentRemoveAction(e, this.name)),
                  this._disconnectSystem(),
                  !0);
            }
            _disconnectSystem() {
              for (let e of this._components.values()) e._disconnectSystem();
              for (let e of this._constraints.values())
                this._system.removeConstraint(e);
              this._system = null;
            }
            addComponent(e) {
              null == e._owner
                ? null == e._system
                  ? this._components.has(e._name)
                    ? r.hdconsole.error(
                        `Tried to add the component whose name ${e._name} already exists`
                      )
                    : ((e._owner = this),
                      this._components.set(e._name, e),
                      null != this._system && e.connectSystem(this._system))
                  : r.hdconsole.error(
                      `Tried to add to component ${this._name} the component ${e._name} that is connected to a system`
                    )
                : r.hdconsole.error(
                    `Tried to add component ${e._name} that already has an owner to component ${this._name}`
                  );
            }
            emplaceVariable(e, t) {
              var n;
              r.hdconsole.assert(
                !this._varRefs.has(e),
                `Trying to add variable ${e} twice to component ${this._name}`
              );
              let o = (0, a.mkVariable)(this, t);
              this._varRefs.set(e, o),
                this._varRefNames.set(o, e),
                this._vars.add(o.value);
              let s = "__stay__" + e,
                u = new i.default(this, (0, a.stayConstraintSpec)(), [o], !0);
              return (
                this._constraints.set(s, u),
                this._constraintNames.set(u, s),
                null === (n = this.system) ||
                  void 0 === n ||
                  n.recorder.record(
                    new l.AssignToConstantAction(
                      this.system,
                      h.VariableReferenceHolder.from(o),
                      t
                    )
                  ),
                o
              );
            }
            emplaceVariableReference(e) {
              r.hdconsole.assert(
                !this._varRefs.has(e),
                `Trying to add variable reference ${e} twice to component ${this._name}`
              );
              let t = new o.default(this);
              return this._varRefs.set(e, t), this._varRefNames.set(t, e), t;
            }
            emplaceConstraint(e, t, n, r = !1) {
              let o = new i.default(this, t, n, r),
                s = null != e ? e : this._nextFreshConstraintName();
              if (this._constraints.has(s))
                throw `Constraint ${s} already exists in component ${this._name}`;
              return (
                this._constraints.set(s, o), this._constraintNames.set(o, s), o
              );
            }
            _nextFreshConstraintName() {
              return "c_" + this._nextFreshCIndex++;
            }
            variableReferenceName(e) {
              let t = this._varRefNames.get(e);
              return null == t ? "<unnamed>" : t;
            }
            hasVariableReferenceName(e) {
              return this._varRefNames.has(e);
            }
            constraintName(e) {
              return this._constraintNames.get(e);
            }
            getVariableReference(e) {
              let t = this._varRefs.get(e);
              return null != t
                ? t
                : null != this._owner
                ? this._owner.getVariableReference(e)
                : void 0;
            }
            get vs() {
              const e = this;
              function t(t) {
                return e._varRefs.get(t);
              }
              return new Proxy(
                {},
                {
                  get: function (e, n) {
                    return t(n);
                  },
                  set: function (n, s, i) {
                    let a = t(s);
                    return null == a
                      ? (r.hdconsole.warn(`Linking an unknown variable ${s}`),
                        !1)
                      : (r.hdconsole.assert(
                          i instanceof o.default,
                          "Can only link value to an ObservableReference (i.e. a VariableReference)"
                        ),
                        null === (l = e.system) ||
                          void 0 === l ||
                          l.recorder.record(
                            new u.LinkReferenceAction(
                              e.system,
                              h.VariableReferenceHolder.from(a),
                              h.VariableReferenceHolder.from(i)
                            )
                          ),
                        a.link(i),
                        !0);
                    var l;
                  },
                }
              );
            }
            get cs() {
              const e = this;
              return new Proxy(
                {},
                {
                  get: function (t, n) {
                    return e._constraints.get(n);
                  },
                }
              );
            }
            get components() {
              const e = this;
              return new Proxy(
                {},
                {
                  get: function (t, n) {
                    return e._components.get(n);
                  },
                }
              );
            }
          }
          t.default = p;
        },
        916: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.default = void 0);
          var r = n(775),
            o = (s(n(137)), s(n(831)), n(268));
          function s(e) {
            return e && e.__esModule ? e : { default: e };
          }
          t.default = class {
            constructor(e) {
              (this._methods = new Set(e)),
                r.hdconsole.assert(
                  this._methods.size > 0,
                  "Constraint specification must have at least one method."
                );
              const t = (0, o.first)(this._methods);
              if (null == t)
                throw "Constraint specification must have at least one method.";
              const n = t.nvars;
              (this._nvars = n), (this._v2ins = []);
              for (let e = 0; e < n; ++e) this._v2ins.push([]);
              for (let e of this._methods) {
                r.hdconsole.assert(
                  e.nvars === n,
                  "All methods in constraint specification must have the same number of variables"
                );
                for (let t of e.outs()) this._v2ins[t].push(e);
              }
            }
            get nvars() {
              return this._nvars;
            }
            ins(e) {
              return this._v2ins[e];
            }
            insSize(e) {
              return this._v2ins[e].length;
            }
            methods() {
              return this._methods;
            }
            hasMethod(e) {
              return this._methods.has(e);
            }
            prettyPrint(e, t) {
              let n = "";
              for (let r of this._methods) n += e + r.prettyPrint(t) + "\n";
              return n;
            }
          };
        },
        292: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.enforceStayMethod = function (e) {
              e.selectedMethod = (0, a.first)(e._cspec._methods);
            }),
            (t.freshComponentName = void 0),
            (t.freshNameGenerator = c),
            (t.isConstraint = function (e) {
              return e.hasOwnProperty("_cspec");
            }),
            (t.isMethod = function (e) {
              return e.hasOwnProperty("_promiseMask");
            }),
            (t.isOwnerOf = function (e, t) {
              let n = t;
              for (; e !== n; ) {
                if (null == n.owner) return !1;
                n = n.owner;
              }
              return !0;
            }),
            (t.isStay = function (e) {
              const t = (0, a.first)(e._cspec._methods);
              return (
                null != t && 1 === t.nvars && "a => a" === t.code.toString()
              );
            }),
            (t.isVariable = function (e) {
              return e.hasOwnProperty("_activation");
            }),
            (t.maskUpdate =
              t.maskPromiseUpdate =
              t.maskPromise =
              t.maskNone =
                void 0),
            (t.mkVariable = function (e, t) {
              let n = new r.default(e);
              return (n.value = new o.default(n, t)), n;
            }),
            (t.stayConstraintSpec = function () {
              return new i.default([
                new s.default(1, [0], [0], [0], (e) => e, "stay"),
              ]);
            }),
            (t.toMask = function (e) {
              switch (e) {
                case 0:
                default:
                  return 0;
                case 1:
                  return 1;
                case 2:
                  return 2;
              }
            }),
            (t.variableFromReference = function (e) {
              let t = e.value;
              if (null == t)
                throw new Error(
                  `Variable reference '${e.qualifiedName}' is dangling`
                );
              return (
                u.hdconsole.assert(
                  !t.stale,
                  `Variable '${e.qualifiedName}' is stale`
                ),
                t
              );
            }),
            l(n(894));
          var r = l(n(831)),
            o = l(n(927)),
            s = (l(n(779)), l(n(137))),
            i = l(n(916)),
            a = n(268),
            u = n(775);
          function l(e) {
            return e && e.__esModule ? e : { default: e };
          }
          function c(e) {
            let t = 0;
            return () => e + t++;
          }
          const h = c("comp_");
          (t.freshComponentName = h),
            (t.maskNone = 0),
            (t.maskPromise = 1),
            (t.maskUpdate = 2),
            (t.maskPromiseUpdate = 3);
        },
        98: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.default = void 0);
          var r = n(17),
            o = n(329),
            s = n(775),
            i = m(n(894)),
            a = (m(n(137)), m(n(927)), n(391)),
            u = m(n(341)),
            l = n(292),
            c = n(268),
            h = n(806),
            d = n(43),
            f = n(80),
            p = n(794);
          function m(e) {
            return e && e.__esModule ? e : { default: e };
          }
          function _(e, t) {
            if (t.has(e))
              throw new TypeError(
                "Cannot initialize the same private elements twice on an object"
              );
          }
          function v(e, t, n) {
            if (!t.has(e))
              throw new TypeError(
                "attempted to " + n + " private field on non-instance"
              );
            return t.get(e);
          }
          var b = new WeakMap(),
            y = new WeakSet();
          function g(e) {
            e &&
              (function (e, t) {
                return t.get ? t.get.call(e) : t.value;
              })(this, v(this, b, "get")) &&
              ((function (e, t, n) {
                (function (e, t, n) {
                  if (t.set) t.set.call(e, n);
                  else {
                    if (!t.writable)
                      throw new TypeError(
                        "attempted to set read only private field"
                      );
                    t.value = n;
                  }
                })(e, v(e, t, "set"), n);
              })(this, b, !1),
              this.recorder.startRecording());
          }
          t.default = class {
            constructor() {
              var e;
              _(this, (e = y)),
                e.add(this),
                (function (e, t, n) {
                  _(e, t), t.set(e, { writable: !0, value: !0 });
                })(this, b),
                (this._optionals = new c.PriorityList()),
                (this._musts = new Set()),
                (this._dirty = new Set()),
                (this._v2cs = new c.OneToManyMap()),
                (this._usg = new a.UpstreamSolutionGraph(this)),
                (this._dsg = new a.DownstreamSolutionGraph(this)),
                (this._recorder = new h.ScriptRecorder(!1)),
                (this._componentNames = new Set());
            }
            get recorder() {
              return this._recorder;
            }
            resetRecorder() {
              const e = this._recorder;
              return (this._recorder = new h.ScriptRecorder(e.recording)), e;
            }
            async parseRecorderDSL(e, t = !0, n = !0) {
              let r = (0, f.buildScriptRecorder)(this, [e]);
              return (
                !0 === n && this.recorder.merge(r),
                !0 === t && (await r.replay()),
                this.updateDirty(),
                r
              );
            }
            get edit() {
              const e = this,
                t = "_self";
              return new Proxy(
                {},
                {
                  get: (n, r, o) => {
                    if ("symbol" == typeof r)
                      throw `cannot access property by symbol ${String(r)}`;
                    const s = r.endsWith(t);
                    let i = s ? r.substring(0, r.length - t.length) : r;
                    const a = e.componentByName(i);
                    if (null == a)
                      throw `Unknown component '${r.toString()}'. Known components: ${[
                        ...e.allComponents(),
                      ]
                        .map((e) => e.name)
                        .join(
                          ", "
                        )}. Add suffix '_self' to a valid component name return the component`;
                    return s ? a : a.edit;
                  },
                  set: (t, n, r, o) =>
                    r instanceof i.default && (e.addComponent(r), !0),
                  ownKeys: function (t, n) {
                    return [...e.allComponents()].map((e) => e.name);
                  },
                  has: function (t, n) {
                    return n in [...e.allComponents()];
                  },
                }
              );
            }
            *allComponents() {
              const e = new Set();
              for (const t of this.allConstraints()) {
                const n = t.owner;
                null == n || e.has(n) || (e.add(n), yield n);
              }
            }
            componentByName(e) {
              for (let t of this.allComponents()) if (t.name === e) return t;
              return null;
            }
            *allConstraints() {
              for (let e of this._musts) yield e;
              for (let e of this._optionals.entries()) yield e;
            }
            addComponent(e) {
              return e.connectSystem(this);
            }
            removeComponent(e) {
              return e.system !== this
                ? (s.hdconsole.warn(
                    `Tried to remove component ${e._name} from a constraint system in which it does not exist.`
                  ),
                  !1)
                : e.disconnectSystem();
            }
            prettyPrint() {
              let e = "-- musts --\n";
              for (let t of this._musts) e += t.prettyPrint();
              e += "-- optionals --\n";
              for (let t of this._optionals.entries()) e += t.prettyPrint();
              return e;
            }
            update(e = !1) {
              this.plan(this.allConstraints()),
                this.solveFromConstraints(this.allConstraints()),
                (function (e, t, n) {
                  if (!t.has(e))
                    throw new TypeError(
                      "attempted to get private field on non-instance"
                    );
                  return n;
                })(this, y, g).call(this, e);
            }
            variables() {
              return this._v2cs.keys();
            }
            constraints(e) {
              return this._v2cs.values(e);
            }
            isMust(e) {
              return this._musts.has(e);
            }
            isOptional(e) {
              return !this._musts.has(e);
            }
            addConstraint(e) {
              for (let t of e.variables()) this._v2cs.add(t, e);
              e._optional ? this._optionals.pushBack(e) : this._musts.add(e),
                this._dirty.add(e);
            }
            removeConstraint(e) {
              for (let t of e.variables())
                this._v2cs.remove(t, e),
                  0 === this._v2cs.count(t) && this._v2cs.removeKey(t);
              e._optional ? this._optionals.remove(e) : this._musts.delete(e),
                this._dirty.delete(e);
            }
            setDirty(e) {
              this._dirty.add(e);
            }
            gatherUpstreamConstraints(e) {
              return (0, c.filter)(
                (e) => (0, l.isConstraint)(e),
                (0, r.reverseTopoSortFrom)(this._usg, e)
              );
            }
            gatherDownstreamConstraints(e) {
              return (0, c.filter)(
                (e) => (0, l.isConstraint)(e),
                (0, r.reverseTopoSortFrom)(this._dsg, e)
              );
            }
            gatherDownstreamVariables(e) {
              return (0, c.filter)(
                (e) => (0, l.isVariable)(e),
                (0, r.reverseTopoSortFrom)(this._dsg, e)
              );
            }
            promoteVariable(e) {
              this.promoteConstraint(this.getStay(e));
            }
            promoteConstraint(e) {
              this._optionals.promote(e);
            }
            demoteConstraint(e) {
              this._optionals.demote(e);
            }
            strength(e) {
              return this.isMust(e)
                ? this._optionals.highestPriority + 1
                : this._optionals.priority(e);
            }
            getStay(e) {
              for (let t of this._v2cs.values(e))
                if (t.name === "__stay__" + e.name) return t;
              return (
                s.hdconsole.assert(
                  !1,
                  "Variable's stay constraint does not exists"
                ),
                null
              );
            }
            planDirty() {
              return this.plan(this._dirty);
            }
            updateDirty() {
              this.planDirty(), this.executeDirty(), this._dirty.clear();
            }
            plan(e) {
              let t = (0, c.join)((0, c.map)((e) => e.variables(), e)),
                n = this.gatherUpstreamConstraints(t),
                r = new Set(),
                o = new Set(),
                s = new c.BinaryHeap(
                  (e, t) => this.strength(e) > this.strength(t)
                ),
                i = new u.default();
              if (
                ((0, c.forEach)((e) => {
                  e.clearSelectedMethod(),
                    e._initPruningData(),
                    this.isMust(e) ? (r.add(e), i.addConstraint(e)) : s.push(e);
                }, n),
                !i.plan())
              )
                throw new Error("no plan for must constraints");
              for (; !s.empty(); ) {
                let e = s.pop();
                switch (i.extendPlanIfPossible(e, o)) {
                  case !1:
                    break;
                  case !0:
                    r.add(e), this._prune(e, r, o);
                    break;
                  case null:
                    i.clear();
                    for (let e of r) i.addConstraint(e);
                    i.addConstraint(e),
                      i.plan()
                        ? (r.add(e), this._prune(e, r, o))
                        : (i.removeConstraint(e),
                          e.clearSelectedMethod(),
                          i.undoChangesAfterFailedPlan());
                }
              }
              return r;
            }
            _prune(e, t, n) {
              let r = new Set(),
                o = Array.from(e._forcedVariables, (t) => e.i2v(t));
              for (; o.length > 0; ) {
                let e = o.pop();
                if ((r.add(e), !n.has(e))) {
                  n.add(e);
                  for (let n of this.constraints(e))
                    if (n.isEnforced() && t.has(n))
                      for (let t of n._makeMethodsNonviableByVariable(e))
                        r.has(t) || o.push(t);
                }
              }
            }
            solveFromConstraints(e) {
              this.solve(
                (0, c.join)((0, c.map)((e) => e.downstreamVariables(), e))
              );
            }
            solve(e) {
              let t = [];
              for (let n of e) {
                let e = this.getSourceConstraint(n);
                null != e && t.push(e);
              }
              let n = Array.from(this.gatherDownstreamConstraints(t)).reverse();
              for (let e of n) this.scheduleConstraint(e);
            }
            executeDirty() {
              let e = Array.from(
                this.gatherDownstreamConstraints(this._dirty)
              ).reverse();
              for (let t of e) this.scheduleConstraint(t);
            }
            getSourceConstraint(e) {
              for (let t of this._v2cs.values(e))
                if (t.isOutputVariable(e)) return t;
              return null;
            }
            scheduleConstraint(e) {
              if (!e.isEnforced()) return;
              let t = e.selectedMethod,
                n = Array.from(e.ins(t), (e) => e._activation),
                r = [];
              (0, c.forEach)((e) => {
                r.push(e._activation), e.pushNewActivation();
              }, e.outs(t));
              let s = Array.from(e.outs(t), (e) => e._activation);
              new o.MethodActivation(t.code, t._promiseMask).execute(n, s),
                (0, c.forEach)((e) => (0, c.release)(e), r);
            }
            scheduleCommand(e, t, n, r = !1) {
              let s = Array.from(e, (e) => e._activation),
                i = Array.from(t),
                a = [];
              (0, c.forEach)((e) => {
                a.push(e._activation), e.pushNewActivation(!r);
              }, i);
              let u = Array.from(i, (e) => e._activation);
              r &&
                this.recorder.record(
                  new d.ScheduleCommandAction(
                    this,
                    Array.from(e, (e) =>
                      p.VariableReferenceHolder.from(e.owner)
                    ),
                    Array.from(t, (e) =>
                      p.VariableReferenceHolder.from(e.owner)
                    ),
                    n,
                    !0
                  )
                ),
                new o.MethodActivation(n, Array(s.length).fill(!1)).execute(
                  s,
                  u
                ),
                (0, c.forEach)((e) => (0, c.release)(e), a),
                i.forEach((e) => this.promoteVariable(e)),
                this.plan((0, c.map)((e) => this.getStay(e), i)),
                this.solve(i);
            }
          };
        },
        779: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.default = void 0);
          var r = n(775),
            o = (i(n(98)), i(n(916)), i(n(137)), i(n(831)), i(n(927)), n(292)),
            s = (i(n(894)), n(268));
          function i(e) {
            return e && e.__esModule ? e : { default: e };
          }
          class a {
            prettyPrint() {
              let e = "constraint " + this.name + " {\n";
              e += "  ";
              for (let t of this._varRefs) e += t.prettyPrint() + "; ";
              return (
                (e += "\n"),
                (e += this._cspec.prettyPrint("  ", this._varRefs)),
                (e += "}\n"),
                e
              );
            }
            substitute(e) {
              return new a(this._owner, this._cspec, e, this._optional);
            }
            constructor(e, t, n, s = !1) {
              (this._owner = e),
                (this._cspec = t),
                (this._varRefs = []),
                (this._vars = Array(t.nvars).fill(null)),
                (this._used = Array(t.nvars).fill(!0)),
                (this._danglingCount = t.nvars),
                (this._optional = s),
                (this._indices = new Map());
              let i = 0;
              for (let e of n) {
                r.hdconsole.assert(
                  (0, o.isOwnerOf)(e._owner, this._owner),
                  `Constructing constraint ${this.name} with a foreign variable reference`
                ),
                  (this._varRefs[i] = e);
                {
                  let t = i;
                  e.subscribe((e) => {
                    if (null !== e) {
                      if (
                        (null != this._vars[t] &&
                          r.hdconsole.error("NOTNULL", this._vars[t]),
                        (this._vars[t] = e),
                        this._indices.set(e, t),
                        0 == --this._danglingCount)
                      ) {
                        let e = this.system;
                        null != e && e.addConstraint(this);
                      }
                    } else {
                      if (null === this._vars[t]) return;
                      if (0 == this._danglingCount++) {
                        let e = this.system;
                        null != e && e.removeConstraint(this);
                      }
                      this._indices.delete(this._vars[t]),
                        (this._vars[t] = null);
                    }
                  });
                }
                ++i;
              }
              this._initPruningData(), this.clearSelectedMethod();
            }
            equals(e) {
              return this === e;
            }
            get name() {
              return null == this._owner
                ? "<unnamed>"
                : this._owner.constraintName(this);
            }
            get owner() {
              return this._owner;
            }
            get optional() {
              return this._optional;
            }
            get nvars() {
              return this._cspec.nvars;
            }
            get system() {
              return this._owner.system;
            }
            i2v(e) {
              return this._vars[e];
            }
            v2i(e) {
              return (
                r.hdconsole.assert(
                  this._indices.has(e),
                  "unknown variable in Constraint.v2i: " + e.name
                ),
                this._indices.get(e)
              );
            }
            variables() {
              return this._vars;
            }
            outs(e) {
              return e.vOuts(this._vars);
            }
            ins(e) {
              return e.vIns(this._vars);
            }
            nonOuts(e) {
              return e.vNonOuts(this._vars);
            }
            methods() {
              return this._cspec.methods();
            }
            hasMethod(e) {
              return this._cspec.hasMethod(e);
            }
            viableMethods() {
              return this._viableMethods;
            }
            get selectedMethod() {
              return this._selectedMethod;
            }
            set selectedMethod(e) {
              r.hdconsole.assert(
                this._cspec.hasMethod(e),
                "selecting a method not in the constraint"
              ),
                (this._selectedMethod = e);
              for (let t of e.ins()) this._used[t] = !0;
            }
            clearSelectedMethod() {
              this._selectedMethod = null;
            }
            isEnforced() {
              return null != this._selectedMethod;
            }
            isOutputVariable(e) {
              return (
                null != this.selectedMethod &&
                this.selectedMethod.isOut(this.v2i(e))
              );
            }
            isInputVariable(e) {
              return (
                null != this.selectedMethod &&
                this.selectedMethod.isIn(this.v2i(e))
              );
            }
            isNonOutputVariable(e) {
              return (
                null != this.selectedMethod &&
                this.selectedMethod.isNonOut(this.v2i(e))
              );
            }
            downstreamVariables() {
              return null == this.selectedMethod
                ? (0, s.mkEmptyIterable)()
                : this.selectedMethod.vOuts(this._vars);
            }
            downstreamAndUndetermined() {
              return null == this.selectedMethod
                ? this._vars
                : this.selectedMethod.vOuts(this._vars);
            }
            upstreamVariables() {
              return null == this.selectedMethod
                ? (0, s.mkEmptyIterable)()
                : this.selectedMethod.vNonOuts(this._vars);
            }
            upstreamAndUndetermined() {
              return null == this.selectedMethod
                ? this._vars
                : this.selectedMethod.vNonOuts(this._vars);
            }
            _initPruningData() {
              (this._viableMethods = new Set()),
                (this._forcedVariables = new Set()),
                (this._outCounts = Array(this.nvars).fill(0));
              for (let e of this.methods()) {
                this._viableMethods.add(e);
                for (let t of e.outs()) this._outCounts[t] += 1;
              }
              const e = this._viableMethods.size;
              for (let t = 0; t < this.nvars; ++t)
                this._outCounts[t] === e && this._forcedVariables.add(t);
            }
            _makeMethodViable(e) {
              r.hdconsole.assert(
                !this._viableMethods.has(e),
                "making a method viable twice"
              ),
                r.hdconsole.assert(this.hasMethod(e), "unknown method");
              let t = this._viableMethods.size;
              this._viableMethods.add(e);
              let n = [];
              for (let t of e.outs()) this._outCounts[t] += 1;
              for (let r of e.nonOuts())
                this._outCounts[r] === t &&
                  (this._forcedVariables.delete(r), n.push(r));
              return n;
            }
            _makeMethodNonviable(e) {
              r.hdconsole.assert(
                this._viableMethods.has(e),
                "making a method nonviable twice"
              ),
                r.hdconsole.assert(
                  this._viableMethods.size >= 2,
                  "trying to make the last viable method nonviable"
                ),
                this._viableMethods.delete(e);
              let t = this._viableMethods.size,
                n = [];
              for (let t of e.outs()) this._outCounts[t] -= 1;
              for (let r of e.nonOuts())
                this._outCounts[r] === t &&
                  (this._forcedVariables.add(r), n.push(r));
              for (let e = 0; e < this._outCounts.length; ++e)
                r.hdconsole.assert(this._outCounts[e] >= 0);
              return n;
            }
            _makeMethodsNonviableByVariable(e) {
              let t = this.v2i(e);
              if (this._forcedVariables.has(t)) return (0, s.mkEmptyIterable)();
              let n = new Set();
              for (let e of (0, s.filter)(
                (e) => e.isOut(t),
                this._viableMethods
              ))
                for (let t of (0, s.map)(
                  (e) => this.i2v(e),
                  this._makeMethodNonviable(e)
                ))
                  n.add(t);
              return n;
            }
          }
          t.default = a;
        },
        137: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.default = void 0);
          var r = n(775),
            o = n(268);
          t.default = class {
            constructor(e, t, n, o, s, i) {
              (this._ins = new Set(t)), (this._outs = new Set(n));
              let a = Array.from(Array(e).keys());
              this._outs.forEach((e) => (a[e] = null)),
                (this._nonOuts = new Set());
              for (let e of a) null != e && this._nonOuts.add(e);
              (this._promiseMask = Array.from(o)),
                r.hdconsole.assert(
                  this._ins.size === this._promiseMask.length,
                  "Number of input arguments and length of promise mask must be the same"
                ),
                (this.code = s),
                null != i && (this._name = i);
            }
            get name() {
              return null == this._name ? "" : this._name;
            }
            get nvars() {
              return this._outs.size + this._nonOuts.size;
            }
            validIndex(e) {
              return e >= 0 && e < this.nvars;
            }
            ins() {
              return this._ins;
            }
            outs() {
              return this._outs;
            }
            nonOuts() {
              return this._nonOuts;
            }
            vIns(e) {
              return (0, o.map)((t) => e[t], this._ins);
            }
            vOuts(e) {
              return (0, o.map)((t) => e[t], this._outs);
            }
            vNonOuts(e) {
              return (0, o.map)((t) => e[t], this._nonOuts);
            }
            nIns() {
              return this._ins.size;
            }
            nOuts() {
              return this._outs.size;
            }
            nNonOuts() {
              return this._nonOuts.size;
            }
            isIn(e) {
              return this._ins.has(e);
            }
            isOut(e) {
              return this._outs.has(e);
            }
            isNonOut(e) {
              return this._nonOuts.has(e);
            }
            prettyPrint(e) {
              let t = this.name + "(";
              for (const n of this._nonOuts) t += e[n].name + " ";
              t += "->";
              for (const n of this._outs) t += " " + e[n].name;
              return (t += ");"), t;
            }
          };
        },
        986: (__unused_webpack_module, exports, __webpack_require__) => {
          Object.defineProperty(exports, "__esModule", { value: !0 }),
            (exports.componentFromJson = componentFromJson),
            (exports.componentToJson = componentToJson),
            (exports.constraintFromJson = constraintFromJson),
            (exports.constraintSpecFromJson = constraintSpecFromJson),
            (exports.constraintSpecToJson = constraintSpecToJson),
            (exports.constraintSystemFromJson = constraintSystemFromJson),
            (exports.constraintSystemToJson = constraintSystemToJson),
            (exports.constraintToJson = constraintToJson),
            (exports.methodFromJson = methodFromJson),
            (exports.methodToJson = methodToJson),
            (exports.variableReferenceToJson = variableReferenceToJson);
          var _method = _interopRequireDefault(__webpack_require__(137)),
            _component = _interopRequireDefault(__webpack_require__(894)),
            _constraintSpec = _interopRequireDefault(__webpack_require__(916)),
            _constraint = _interopRequireDefault(__webpack_require__(779)),
            _constraintSystem = _interopRequireDefault(__webpack_require__(98)),
            _constraintSystemUtil = __webpack_require__(292),
            _variableReference = _interopRequireDefault(
              __webpack_require__(831)
            );
          function _interopRequireDefault(e) {
            return e && e.__esModule ? e : { default: e };
          }
          function variableReferenceToJson(e) {
            var t, n, r;
            return {
              owner:
                null !==
                  (t =
                    null === (n = e.value) || void 0 === n
                      ? void 0
                      : n.owner.owner.name) && void 0 !== t
                  ? t
                  : e.owner.name,
              name: e.name,
              value: null === (r = e.value) || void 0 === r ? void 0 : r.value,
            };
          }
          function methodToJson(e) {
            var t;
            return {
              name: e._name,
              nvars: e.nvars,
              ins: Array.from(e._ins),
              outs: Array.from(e._outs),
              code:
                null === (t = e.code) || void 0 === t ? void 0 : t.toString(),
              promiseMask: Array.from(e._promiseMask),
            };
          }
          function methodFromJson(method) {
            return new _method.default(
              method.nvars,
              method.ins,
              method.outs,
              method.promiseMask.map(_constraintSystemUtil.toMask),
              eval(`let f = ${method.code}; f`),
              method.name
            );
          }
          function constraintSpecToJson(e) {
            return { methods: Array.from(e._methods).map(methodToJson) };
          }
          function constraintSpecFromJson(e) {
            return new _constraintSpec.default(e.methods.map(methodFromJson));
          }
          function componentToJson(e, t) {
            return {
              name: e.name,
              variables: Array.from(e._varRefs.values()).map((e) =>
                variableReferenceToJson(e)
              ),
              constraints: t,
            };
          }
          function componentFromJson(e) {
            let t = new _component.default(e.name),
              n = [];
            return (
              e.variables.forEach((r) => {
                if (r.owner === e.name) t.emplaceVariable(r.name, r.value);
                else {
                  let e = t.emplaceVariableReference(r.name);
                  n.push(() => {
                    var n, o, s;
                    e.value =
                      null === (n = t.system) ||
                      void 0 === n ||
                      null === (o = n.componentByName(r.owner)) ||
                      void 0 === o ||
                      null === (s = o.getVariableReference(r.name)) ||
                      void 0 === s
                        ? void 0
                        : s.value;
                  });
                }
              }),
              e.constraints.forEach((e) => {
                constraintFromJson(e, t);
              }),
              [t, n]
            );
          }
          function constraintToJson(e, t) {
            return {
              name: t,
              constraintSpec: constraintSpecToJson(e._cspec),
              optional: e._optional,
            };
          }
          function constraintFromJson(e, t) {
            return t.emplaceConstraint(
              e.name,
              constraintSpecFromJson(e.constraintSpec),
              t._varRefs.values(),
              e.optional
            );
          }
          function constraintSystemToJson(e) {
            let t = new Map();
            for (const n of e.allComponents())
              t.set(
                n,
                [...n._constraints.values()].filter((e) => !e.optional)
              );
            const n = {
              components: Array.from(t.keys()).map((e) => {
                var n;
                const r = (
                  null !== (n = t.get(e)) && void 0 !== n ? n : []
                ).map((t) => {
                  var n;
                  return constraintToJson(
                    t,
                    null !== (n = e.constraintName(t)) && void 0 !== n ? n : ""
                  );
                });
                return componentToJson(e, r);
              }),
            };
            return JSON.stringify(n);
          }
          function constraintSystemFromJson(e) {
            const t = JSON.parse(e),
              n = new _constraintSystem.default(),
              r = [];
            return (
              t.components.forEach((e) => {
                let [t, o] = componentFromJson(e);
                n.addComponent(t), r.push(...o);
              }),
              r.forEach((e) => e()),
              n
            );
          }
        },
        341: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.default = void 0),
            s(n(927)),
            s(n(779)),
            s(n(137));
          var r = n(775),
            o = n(268);
          function s(e) {
            return e && e.__esModule ? e : { default: e };
          }
          t.default = class {
            constructor() {
              (this._v2cs = new o.OneToManyMap()),
                (this._freeVars = new Set()),
                (this._forcedByPlan = new Map()),
                (this._changeList = []);
            }
            addConstraint(e) {
              for (let t of e.variables())
                switch ((this._v2cs.add(t, e), this._v2cs.count(t))) {
                  case 1:
                    this._freeVars.add(t);
                    break;
                  case 2:
                    this._freeVars.delete(t);
                }
            }
            removeConstraint(e) {
              for (let t of e.variables())
                switch ((this._v2cs.remove(t, e), this._v2cs.count(t))) {
                  case 0:
                    this._freeVars.delete(t), this._v2cs.removeKey(t);
                    break;
                  case 1:
                    this._freeVars.add(t);
                }
            }
            clear() {
              this._v2cs.clear(), this._freeVars.clear();
            }
            plan() {
              let e = !0;
              for (; e; ) {
                e = !1;
                for (let [t, n] of this.freeMethods()) {
                  e = !0;
                  let r = t.selectedMethod;
                  if (r !== n && null != r) {
                    this._changeList.push([t, r]);
                    for (let e of t.outs(r)) this._forcedByPlan.delete(e);
                  }
                  t.selectedMethod = n;
                  for (let e of t.outs(n)) this._forcedByPlan.set(e, n);
                  this.removeConstraint(t);
                }
              }
              return (
                0 === this._v2cs.countKeys() && ((this._changeList = []), !0)
              );
            }
            undoChangesAfterFailedPlan() {
              for (let [e, t] of this._changeList) {
                let n = e.selectedMethod;
                if (null != n)
                  for (let t of e.outs(n)) this._forcedByPlan.delete(t);
                for (let n of e.outs(t)) this._forcedByPlan.set(n, t);
                e.selectedMethod = t;
              }
            }
            extendPlanIfPossible(e, t) {
              if (
                (0, o.every)(
                  (n) => (0, o.some)((e) => t.has(e), e.outs(n)),
                  e._viableMethods
                )
              )
                return !1;
              if (
                (0, o.every)((e) => !this._forcedByPlan.has(e), e.variables())
              ) {
                let t = (0, o.first)(e._viableMethods);
                if (
                  (r.hdconsole.assert(
                    null != t,
                    "constraint has no viable methods"
                  ),
                  null == t)
                )
                  return !1;
                e.selectedMethod = t;
                for (let n of e.outs(t)) this._forcedByPlan.set(n, t);
                return !0;
              }
              return null;
            }
            *freeMethods() {
              let e = new Set();
              for (let t of this._freeVars)
                for (let n of this._v2cs.values(t))
                  if (!e.has(n)) {
                    e.add(n);
                    for (let e of n.viableMethods())
                      if (
                        (0, o.every)((e) => this._freeVars.has(e), n.outs(e))
                      ) {
                        yield [n, e];
                        break;
                      }
                  }
            }
            firstFreeMethod() {
              return (0, o.first)(this.freeMethods());
            }
            constraints() {
              let e = new Set();
              for (let t of this._v2cs.keys())
                for (let n of this._v2cs.values(t)) e.add(n);
              return e;
            }
          };
        },
        391: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.UpstreamSolutionGraph = t.DownstreamSolutionGraph = void 0),
            s(n(779)),
            s(n(927)),
            s(n(98));
          var r = n(292),
            o = n(268);
          function s(e) {
            return e && e.__esModule ? e : { default: e };
          }
          (t.DownstreamSolutionGraph = class {
            constructor(e) {
              this._system = e;
            }
            adjacentOutVertices(e) {
              return (0, r.isConstraint)(e)
                ? e.downstreamAndUndetermined()
                : (0, o.filter)(
                    (t) => t.isNonOutputVariable(e) || !t.isEnforced(),
                    this._system._v2cs.values(e)
                  );
            }
            outDegree(e) {
              return (0, r.isConstraint)(e)
                ? null != e.selectedMethod
                  ? e.selectedMethod.nOuts()
                  : 0
                : (0, o.count)(this.adjacentOutVertices(e));
            }
          }),
            (t.UpstreamSolutionGraph = class {
              constructor(e) {
                this._system = e;
              }
              adjacentOutVertices(e) {
                return (0, r.isConstraint)(e)
                  ? e.upstreamAndUndetermined()
                  : (0, o.filter)(
                      (t) => t.isOutputVariable(e) || !t.isEnforced(),
                      this._system._v2cs.values(e)
                    );
              }
              outDegree(e) {
                return (0, r.isConstraint)(e)
                  ? null != e.selectedMethod
                    ? e.selectedMethod.nNonOuts()
                    : 0
                  : (0, o.count)(this.adjacentOutVertices(e));
              }
            });
        },
        831: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.default = void 0);
          var r = n(268);
          function o(e) {
            return e && e.__esModule ? e : { default: e };
          }
          o(n(894)), o(n(98)), o(n(927));
          class s extends r.ObservableReference {
            constructor(e) {
              super(), (this._owner = e);
            }
            get system() {
              return this._owner.system;
            }
            get name() {
              return this._owner.variableReferenceName(this);
            }
            get owner() {
              return this._owner;
            }
            get owningReference() {
              var e, t;
              return null !==
                (e =
                  null === (t = this.value) || void 0 === t
                    ? void 0
                    : t._owner) && void 0 !== e
                ? e
                : null;
            }
            get qualifiedName() {
              return `${this.owner.name}.${this.name}`;
            }
            isOwningReference() {
              return null != this.value && this.value._owner === this;
            }
            prettyPrint() {
              let e = this.name;
              return null != this.value && (e += ":" + this.value._index), e;
            }
          }
          t.default = s;
        },
        927: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.default = void 0);
          var r = n(329),
            o = (function (e, t) {
              if (e && e.__esModule) return e;
              if (
                null === e ||
                ("object" != typeof e && "function" != typeof e)
              )
                return { default: e };
              var n = l(t);
              if (n && n.has(e)) return n.get(e);
              var r = {},
                o = Object.defineProperty && Object.getOwnPropertyDescriptor;
              for (var s in e)
                if (
                  "default" !== s &&
                  Object.prototype.hasOwnProperty.call(e, s)
                ) {
                  var i = o ? Object.getOwnPropertyDescriptor(e, s) : null;
                  i && (i.get || i.set)
                    ? Object.defineProperty(r, s, i)
                    : (r[s] = e[s]);
                }
              return (r.default = e), n && n.set(e, r), r;
            })(n(534)),
            s = (u(n(831)), u(n(98)), n(268)),
            i = n(269),
            a = n(794);
          function u(e) {
            return e && e.__esModule ? e : { default: e };
          }
          function l(e) {
            if ("function" != typeof WeakMap) return null;
            var t = new WeakMap(),
              n = new WeakMap();
            return (l = function (e) {
              return e ? n : t;
            })(e);
          }
          let c = (() => {
            let e = 0;
            return () => ++e;
          })();
          t.default = class {
            get value() {
              return this._inError ? void 0 : this._value;
            }
            get error() {
              return this._inError ? this._error : void 0;
            }
            get stale() {
              return !this._pending && this._inError;
            }
            get pending() {
              return this._pending;
            }
            constructor(e, t) {
              (this._index = c()),
                (this._owner = e),
                (this._value = void 0),
                (this._error = void 0),
                (this._inError = !1),
                (this._pending = !0),
                (this._subject = new o.Subject((e) => {
                  let t = {
                    value: !1 === this._pending ? this._value : void 0,
                    pending: this._pending,
                    inError: this._inError,
                  };
                  this._inError && (t.error = this._error), e.next(t);
                })),
                this.pushNewActivation(),
                void 0 !== t &&
                  (this._activation.resolve(t), (this._value = t));
            }
            subscribe(e) {
              return this._subject.subscribe(e);
            }
            subscribeValue(e) {
              return this._subject
                .filter((e) => e.hasOwnProperty("value"))
                .map((e) => e.value)
                .subscribe(e);
            }
            subscribePending(e) {
              return this._subject
                .filter((e) => e.hasOwnProperty("pending"))
                .map((e) => e.pending)
                .subscribe(e);
            }
            subscribeError(e) {
              return this._subject
                .filter(
                  (e) =>
                    e.hasOwnProperty("inError") || e.hasOwnProperty("error")
                )
                .map((e) => {
                  let t = {};
                  return (
                    e.hasOwnProperty("error") && (t.error = e.error),
                    e.hasOwnProperty("inError") && (t.inError = e.inError),
                    t
                  );
                })
                .subscribe(e);
            }
            pushNewActivation(e = !1) {
              var t;
              let n = new r.VariableActivation(this);
              (this._activation = n),
                !1 === this._pending &&
                  ((this._pending = !0),
                  this._subject.sendNext({ pending: !0 }));
              const o = this.system,
                s = this.owner,
                u =
                  null !== (t = null == o ? void 0 : o.recorder.recording) &&
                  void 0 !== t &&
                  t;
              return (
                n.promise.then(
                  (t) => {
                    if (
                      (!0 === e &&
                        !0 === u &&
                        (null == o ||
                          o.recorder.record(
                            new i.AssignToConstantAction(
                              o,
                              a.VariableReferenceHolder.from(s),
                              t
                            )
                          )),
                      n === this._activation)
                    ) {
                      this._value = t;
                      let e = { value: t };
                      this._inError && ((this._inError = !1), (e.inError = !1)),
                        (this._pending = !1),
                        (e.pending = !1),
                        this._subject.sendNext(e);
                    }
                  },
                  (e) => {
                    if (n === this._activation) {
                      this._error = e;
                      let t = { error: e };
                      this._inError || ((this._inError = !0), (t.inError = !0)),
                        (this._pending = !1),
                        (t.pending = !1),
                        this._subject.sendNext(t);
                    }
                  }
                ),
                n
              );
            }
            get currentPromise() {
              return this._activation.promise;
            }
            get owner() {
              return this._owner;
            }
            get system() {
              return this._owner.system;
            }
            get name() {
              return null == this._owner ? "<unnamed>" : this._owner.name;
            }
            set(e) {
              var t;
              null === (t = this.system) ||
                void 0 === t ||
                t.recorder.record(
                  new i.AssignToConstantAction(
                    this.system,
                    a.VariableReferenceHolder.from(this.owner),
                    e
                  )
                ),
                this.assign(e);
              let n = this.system;
              return null != n && n.updateDirty(), this;
            }
            fail(e) {
              this._assign().reject(e);
              let t = this.system;
              return null != t && t.updateDirty(), this;
            }
            assign(e) {
              this._assign().resolve(e);
            }
            _assign() {
              let e = this._activation;
              this.pushNewActivation();
              let t = this.system;
              if (null != t) {
                t.promoteVariable(this);
                let e = t.getSourceConstraint(this);
                null != e && t.setDirty(e), t.setDirty(t.getStay(this));
              }
              return (0, s.release)(e), this._activation;
            }
            touch() {
              let e = this.system;
              return null != e && e.promoteVariable(this), this;
            }
            _touchPlanSolve() {
              this.touch();
              let e = this.system,
                t = this._activation;
              if ((this.pushNewActivation(), null == e))
                return (0, s.release)(t), this._activation;
              let n = this._activation,
                r = e.getSourceConstraint(this);
              return (
                e.setDirty(null != r ? r : e.getStay(this)),
                e.updateDirty(),
                (0, s.release)(t),
                n
              );
            }
          };
        },
        17: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.dfsVisit = o),
            (t.reverseTopoSort = function (e) {
              return i(e, e.vertices());
            }),
            (t.reverseTopoSortFrom = i);
          var r = n(268);
          function o(e, t, n, o = new Set()) {
            let s = [],
              i = (0, r.iterableToIterator)(t);
            for (;;) {
              let t = i.next();
              if (t.done) {
                if (0 === s.length) break;
                let e;
                ([e, i] = s.pop()), n.finishVertex(e);
                continue;
              }
              let a = t.value;
              o.has(a) ||
                (n.discoverVertex(a),
                o.add(a),
                s.push([a, i]),
                (i = (0, r.iterableToIterator)(e.adjacentOutVertices(a))));
            }
          }
          class s {
            constructor() {
              this.result = [];
            }
            discoverVertex(e) {}
            finishVertex(e) {
              this.result.push(e);
            }
          }
          function i(e, t) {
            let n = new s();
            return o(e, t, n), n.result;
          }
        },
        775: (e, t) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.hdconsole = t.default = t.HdError = t.HdConsole = void 0);
          class n {
            constructor(e) {
              (this._name = e),
                "object" == typeof jest ? this.off() : this.on();
            }
            off() {
              (this.log = (...e) => {}),
                (this.warn = (...e) => {}),
                (this.error = (...e) => {}),
                (this.assert = (e, ...t) => {});
            }
            errorsOnly() {
              this.off(),
                (this.error = console.error),
                (this.assert = console.assert);
            }
            on() {
              (this.log = console.log),
                (this.warn = console.warn),
                (this.error = console.error),
                (this.assert = console.assert);
            }
          }
          t.HdConsole = n;
          const r = new n("HotDrink");
          t.hdconsole = r;
          var o = { hdconsole: r };
          t.default = o;
          class s extends Error {
            constructor(e, ...t) {
              super(e, ...t),
                Error.captureStackTrace && Error.captureStackTrace(this, s),
                (this.name = "HdError");
            }
          }
          t.HdError = s;
        },
        707: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 });
          var r = {
            defaultConstraintSystem: !0,
            constraintSystemInVisNodesAndEdges: !0,
            constraintSystemInVisAllNodesAndEdges: !0,
            ConstraintSystem: !0,
            Method: !0,
            component: !0,
            hdl: !0,
            hdconsole: !0,
            maskPromiseUpdate: !0,
            maskUpdate: !0,
            maskPromise: !0,
            maskNone: !0,
            MaskType: !0,
            Component: !0,
            Constraint: !0,
            ConstraintSpec: !0,
            Variable: !0,
            VariableReference: !0,
            SimplePlanner: !0,
            ScriptRecorder: !0,
            buildScriptRecorder: !0,
            RecordedAction: !0,
            CustomRecordedAction: !0,
          };
          Object.defineProperty(t, "Component", {
            enumerable: !0,
            get: function () {
              return h.default;
            },
          }),
            Object.defineProperty(t, "Constraint", {
              enumerable: !0,
              get: function () {
                return d.default;
              },
            }),
            Object.defineProperty(t, "ConstraintSpec", {
              enumerable: !0,
              get: function () {
                return f.default;
              },
            }),
            Object.defineProperty(t, "ConstraintSystem", {
              enumerable: !0,
              get: function () {
                return o.default;
              },
            }),
            Object.defineProperty(t, "CustomRecordedAction", {
              enumerable: !0,
              get: function () {
                return C.CustomRecordedAction;
              },
            }),
            Object.defineProperty(t, "MaskType", {
              enumerable: !0,
              get: function () {
                return l.MaskType;
              },
            }),
            Object.defineProperty(t, "Method", {
              enumerable: !0,
              get: function () {
                return s.default;
              },
            }),
            Object.defineProperty(t, "RecordedAction", {
              enumerable: !0,
              get: function () {
                return y.RecordedAction;
              },
            }),
            Object.defineProperty(t, "ScriptRecorder", {
              enumerable: !0,
              get: function () {
                return v.ScriptRecorder;
              },
            }),
            Object.defineProperty(t, "SimplePlanner", {
              enumerable: !0,
              get: function () {
                return _.default;
              },
            }),
            Object.defineProperty(t, "Variable", {
              enumerable: !0,
              get: function () {
                return p.default;
              },
            }),
            Object.defineProperty(t, "VariableReference", {
              enumerable: !0,
              get: function () {
                return m.default;
              },
            }),
            Object.defineProperty(t, "buildScriptRecorder", {
              enumerable: !0,
              get: function () {
                return b.buildScriptRecorder;
              },
            }),
            Object.defineProperty(t, "component", {
              enumerable: !0,
              get: function () {
                return i.component;
              },
            }),
            (t.constraintSystemInVisAllNodesAndEdges = function (e) {
              let t = [],
                n = [];
              for (let n of e.variables())
                t.push({ id: R(n), label: n.name, shape: "box" });
              for (let r of e.allConstraints()) {
                let e = 0;
                for (let o of r.methods()) {
                  let s = `m#${R(r)}-${e}`;
                  console.log("ID: ", s, "C.M", r.name + "." + o.name);
                  let i = "lime";
                  t.push({
                    id: s,
                    label: r.name + "." + o.name,
                    cid: "C#" + R(r),
                    color: i,
                  });
                  for (let e of r.nonOuts(o))
                    n.push({ from: R(e), to: s, arrows: "to", dashes: !0 }),
                      console.log("INS: ", e.name);
                  for (let e of r.outs(o))
                    n.push({ from: s, to: R(e), arrows: "to" }),
                      console.log("OUTS: ", e.name);
                  ++e;
                }
              }
              return [t, n];
            }),
            (t.constraintSystemInVisNodesAndEdges = function (e) {
              let t = [],
                n = [];
              for (let n of e.variables())
                t.push({ id: R(n), label: n.name, shape: "box" });
              for (let r of e.allConstraints()) {
                let e = 0;
                if (!r.isEnforced()) continue;
                let o = r.selectedMethod,
                  s = `m#${R(r)}-${e}`;
                console.log("ID: ", s, "C.M", r.name + "." + o.name);
                let i = "lime";
                t.push({
                  id: s,
                  label: r.name + "." + o.name,
                  cid: "C#" + R(r),
                  color: i,
                });
                for (let e of r.nonOuts(o))
                  n.push({ from: R(e), to: s, arrows: "to", dashes: !0 });
                for (let e of r.outs(o))
                  n.push({ from: s, to: R(e), arrows: "to" });
                ++e;
              }
              return [t, n];
            }),
            (t.defaultConstraintSystem = void 0),
            Object.defineProperty(t, "hdconsole", {
              enumerable: !0,
              get: function () {
                return a.hdconsole;
              },
            }),
            Object.defineProperty(t, "hdl", {
              enumerable: !0,
              get: function () {
                return i.hdl;
              },
            }),
            Object.defineProperty(t, "maskNone", {
              enumerable: !0,
              get: function () {
                return l.maskNone;
              },
            }),
            Object.defineProperty(t, "maskPromise", {
              enumerable: !0,
              get: function () {
                return l.maskPromise;
              },
            }),
            Object.defineProperty(t, "maskPromiseUpdate", {
              enumerable: !0,
              get: function () {
                return l.maskPromiseUpdate;
              },
            }),
            Object.defineProperty(t, "maskUpdate", {
              enumerable: !0,
              get: function () {
                return l.maskUpdate;
              },
            });
          var o = k(n(98)),
            s = k(n(137)),
            i = n(230),
            a = n(775),
            u = n(391);
          Object.keys(u).forEach(function (e) {
            "default" !== e &&
              "__esModule" !== e &&
              (Object.prototype.hasOwnProperty.call(r, e) ||
                (e in t && t[e] === u[e]) ||
                Object.defineProperty(t, e, {
                  enumerable: !0,
                  get: function () {
                    return u[e];
                  },
                }));
          });
          var l = n(292);
          Object.keys(l).forEach(function (e) {
            "default" !== e &&
              "__esModule" !== e &&
              (Object.prototype.hasOwnProperty.call(r, e) ||
                (e in t && t[e] === l[e]) ||
                Object.defineProperty(t, e, {
                  enumerable: !0,
                  get: function () {
                    return l[e];
                  },
                }));
          });
          var c = n(986);
          Object.keys(c).forEach(function (e) {
            "default" !== e &&
              "__esModule" !== e &&
              (Object.prototype.hasOwnProperty.call(r, e) ||
                (e in t && t[e] === c[e]) ||
                Object.defineProperty(t, e, {
                  enumerable: !0,
                  get: function () {
                    return c[e];
                  },
                }));
          });
          var h = k(n(894)),
            d = k(n(779)),
            f = k(n(916)),
            p = k(n(927)),
            m = k(n(831)),
            _ = k(n(341)),
            v = n(806),
            b = n(80),
            y = n(211),
            g = n(84);
          Object.keys(g).forEach(function (e) {
            "default" !== e &&
              "__esModule" !== e &&
              (Object.prototype.hasOwnProperty.call(r, e) ||
                (e in t && t[e] === g[e]) ||
                Object.defineProperty(t, e, {
                  enumerable: !0,
                  get: function () {
                    return g[e];
                  },
                }));
          });
          var w = n(673);
          Object.keys(w).forEach(function (e) {
            "default" !== e &&
              "__esModule" !== e &&
              (Object.prototype.hasOwnProperty.call(r, e) ||
                (e in t && t[e] === w[e]) ||
                Object.defineProperty(t, e, {
                  enumerable: !0,
                  get: function () {
                    return w[e];
                  },
                }));
          });
          var S = n(888);
          Object.keys(S).forEach(function (e) {
            "default" !== e &&
              "__esModule" !== e &&
              (Object.prototype.hasOwnProperty.call(r, e) ||
                (e in t && t[e] === S[e]) ||
                Object.defineProperty(t, e, {
                  enumerable: !0,
                  get: function () {
                    return S[e];
                  },
                }));
          });
          var C = n(477);
          function k(e) {
            return e && e.__esModule ? e : { default: e };
          }
          const M = new o.default();
          t.defaultConstraintSystem = M;
          let R = (() => {
            let e = new WeakMap(),
              t = 0;
            return (n) => {
              let r = e.get(n);
              if (void 0 !== r) return r;
              {
                let r = t++;
                return e.set(n, r), r;
              }
            };
          })();
        },
        877: (e, t) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.Position = void 0),
            (t.isAlpha = o),
            (t.isAlphaNum = function (e) {
              return s(e) || r(e);
            }),
            (t.isAlphaPlus = s),
            (t.isDigit = r),
            (t.isWhitespace = function (e) {
              switch (e) {
                case " ":
                case "\t":
                case "\r":
                case "\n":
                  return !0;
                default:
                  return !1;
              }
            });
          class n {
            constructor(e, t, n, r) {
              (this._pos = e),
                (this._row = t),
                (this._col = n),
                void 0 !== r && (this._source = r);
            }
            get pos() {
              return this._pos;
            }
            get row() {
              return this._row;
            }
            get col() {
              return this._col;
            }
            get source() {
              return null != this._source ? this._source : "-";
            }
            toString() {
              return this.source + ":" + this.row + ":" + this.col;
            }
            advance(e) {
              for (let t = 0; t < e.length; ++t)
                "\n" === e[t] ? (this._row++, (this._col = 0)) : this._col++,
                  this._pos++;
            }
            clone() {
              let e = new n(this.pos, this.row, this.col);
              return (
                this.hasOwnProperty("_source") && (e._source = this._source), e
              );
            }
          }
          function r(e) {
            return e >= "0" && e <= "9";
          }
          function o(e) {
            return (e >= "a" && e <= "z") || (e >= "A" && e <= "Z");
          }
          function s(e) {
            return o(e) || "_" === e || "$" === e;
          }
          t.Position = n;
        },
        534: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.SubscriptionObserver =
              t.Subject =
              t.SimpleSubscription =
              t.ReplaySubject =
              t.Observable =
              t.HdSubscription =
              t.BehaviorSubject =
                void 0),
            (t.mkObserver = u);
          var r = n(775);
          function o(e, t, n) {
            !(function (e, t) {
              if (t.has(e))
                throw new TypeError(
                  "Cannot initialize the same private elements twice on an object"
                );
            })(e, t),
              t.set(e, n);
          }
          function s(e, t) {
            return (function (e, t) {
              return t.get ? t.get.call(e) : t.value;
            })(e, a(e, t, "get"));
          }
          function i(e, t, n) {
            return (
              (function (e, t, n) {
                if (t.set) t.set.call(e, n);
                else {
                  if (!t.writable)
                    throw new TypeError(
                      "attempted to set read only private field"
                    );
                  t.value = n;
                }
              })(e, a(e, t, "set"), n),
              n
            );
          }
          function a(e, t, n) {
            if (!t.has(e))
              throw new TypeError(
                "attempted to " + n + " private field on non-instance"
              );
            return t.get(e);
          }
          function u(e, t, n) {
            if ("function" == typeof e) {
              let r = {};
              return (
                (r.next = e),
                null != t && (r.error = t),
                null != n && (r.complete = n),
                r
              );
            }
            return e;
          }
          class l {
            constructor(e) {
              this._subscriber = e;
            }
            subscribe(e, t, n) {
              let r = u(e, t, n),
                o = new p(r);
              try {
                null != r.start && r.start(o);
              } catch (e) {}
              if (o.closed) return o;
              let s = new m(o);
              try {
                let e = this._subscriber(s);
                "object" == typeof e && (e = () => e.unsubscribe()),
                  (o._cleanup = e);
              } catch (e) {
                s.error(e);
              }
              return o.closed && o.cleanup(), o;
            }
            static of(...e) {
              return new l((t) => {
                for (let n of e) if ((t.next(n), t.closed)) return;
                t.complete();
              });
            }
            filter(e) {
              return new l((t) => {
                let n = u(t);
                return (
                  null == n.next && (n = { next: (e) => {} }),
                  this.subscribe((t) => {
                    !0 === e(t) && n.next(t);
                  })
                );
              });
            }
            map(e) {
              return new l((t) => {
                let n = u(t);
                return (
                  null == n.next && (n = { next: (e) => {} }),
                  this.subscribe((n) => {
                    u(t).next(e(n));
                  })
                );
              });
            }
          }
          t.Observable = l;
          var c = new WeakMap(),
            h = new WeakMap(),
            d = new WeakMap();
          class f {
            constructor(e = () => {}) {
              o(this, c, { writable: !0, value: [] }),
                o(this, h, { writable: !0, value: void 0 }),
                o(this, d, { writable: !0, value: !1 }),
                i(this, h, e);
            }
            get closed() {
              return s(this, d);
            }
            get additionalSubscriptions() {
              return s(this, c);
            }
            unsubscribe() {
              i(this, d, !0), s(this, h).call(this);
              for (let e of s(this, c)) e.unsubscribe();
            }
          }
          t.SimpleSubscription = f;
          class p extends f {
            constructor(e) {
              super(), (this._observer = e), (this._cleanup = void 0);
            }
            unsubscribe() {
              this.cleanup(), (this._observer = void 0), super.unsubscribe();
            }
            cleanup() {
              const e = this._cleanup;
              if (void 0 !== e) {
                this._cleanup = void 0;
                try {
                  e();
                } catch (e) {}
              }
            }
          }
          t.HdSubscription = p;
          class m {
            constructor(e) {
              this._subscription = e;
            }
            get closed() {
              return this._subscription.closed;
            }
            next(e) {
              let t = this._subscription;
              if (!t.closed && null != t._observer.next)
                try {
                  t._observer.next(e);
                } catch (e) {
                  r.hdconsole.error(
                    "Exception thrown on subscription next:",
                    e
                  );
                }
            }
            error(e) {
              let t = this._subscription;
              if (!t.closed) {
                if (null != t._observer.error)
                  try {
                    t._observer.error(e);
                  } catch (e) {
                    r.hdconsole.error(
                      "Exception thrown on subscription error: ",
                      e
                    );
                  }
                this._subscription.cleanup();
              }
            }
            complete() {
              let e = this._subscription;
              if (!e.closed) {
                if (null != e._observer.complete)
                  try {
                    e._observer.complete();
                  } catch (e) {
                    r.hdconsole.error(
                      "Exception thrown on subscription complete: ",
                      e
                    );
                  }
                this._subscription.cleanup();
              }
            }
          }
          t.SubscriptionObserver = m;
          class _ extends l {
            constructor(e = (e) => {}) {
              let t = { observers: new Set(), done: !1 };
              super(
                (n) => (
                  t.done || t.observers.add(n),
                  e(n),
                  () => t.observers.delete(n)
                )
              ),
                (this._subjectState = t);
            }
            sendNext(e) {
              if (!this._subjectState.done)
                for (const t of this._subjectState.observers) t.next(e);
            }
            sendError(e) {
              if (!this._subjectState.done) {
                this._subjectState.done = !0;
                for (const t of this._subjectState.observers) t.error(e);
              }
            }
            sendComplete() {
              if (!this._subjectState.done) {
                this._subjectState.done = !0;
                for (const e of this._subjectState.observers) e.complete();
              }
            }
            get nObservers() {
              return this._subjectState.observers.size;
            }
          }
          t.Subject = _;
          class v extends _ {
            constructor(e = 1) {
              let t = { past: [], pastLimit: e, isCompleted: !1, isError: !1 };
              super((e) => {
                for (let n of t.past) e.next(n);
                t.isError ? e.error(t.error) : t.isCompleted && e.complete();
              }),
                (this._replaySubjectState = t);
            }
            sendNext(e) {
              this._replaySubjectState.past.push(e),
                this._replaySubjectState.past.length >
                  this._replaySubjectState.pastLimit &&
                  this._replaySubjectState.past.shift(),
                super.sendNext(e);
            }
            sendError(e) {
              (this._replaySubjectState.isError = !0),
                (this._replaySubjectState.error = e),
                super.sendError(e);
            }
            sendComplete() {
              (this._replaySubjectState.isCompleted = !0), super.sendComplete();
            }
          }
          (t.ReplaySubject = v),
            (t.BehaviorSubject = class extends v {
              constructor(e) {
                super(1), this._replaySubjectState.past.push(e);
              }
            });
        },
        316: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.blockComment =
              t.alphaPlus =
              t.alphaNum =
              t.ParseState =
              t.ParseError =
                void 0),
            (t.context = function (e, t) {
              return (n) => {
                n.pushContext(e);
                try {
                  return t(n);
                } finally {
                  n.popContext();
                }
              };
            }),
            (t.digit = t.currentPos = void 0),
            (t.eof = b),
            (t.exactString = R),
            (t.fail = d),
            (t.failF = f),
            (t.failure = function (e) {
              return d([]);
            }),
            (t.go = V),
            (t.hdl = function (e, ...t) {
              let n = "";
              for (let r = 0; r < t.length; ++r)
                (n += e[r]), (n += "###" + r + "###");
              return e.length > 0 && (n += e[e.length - 1]), c(n, t);
            }),
            (t.identifier = function (e) {
              return T(
                (e, t, n) => e + t.join(""),
                y(o.isAlphaPlus, "<identifier>"),
                k(y(o.isAlphaNum)),
                N
              )(e);
            }),
            (t.ignore = function (e) {
              return T(() => E, e);
            }),
            (t.inBetween = $),
            (t.item = m),
            (t.keyword = function (e) {
              return B(z(q(R(e), B(S, "<alphanum>"))), `<keyword: ${e}>`);
            }),
            (t.lift = T),
            (t.lift2 = function (e) {
              return (t, n) => T(e, t, n);
            }),
            (t.lift3 = function (e) {
              return (t, n, r) => T(e, t, n, r);
            }),
            (t.lift4 = function (e) {
              return (t, n, r, o) => T(e, t, n, r, o);
            }),
            (t.lift5 = function (e) {
              return (t, n, r, o, s) => T(e, t, n, r, o, s);
            }),
            (t.lineComment = void 0),
            (t.many = k),
            (t.many1 = M),
            (t.manyws = void 0),
            (t.mkParseState = c),
            (t.must = function (e, t) {
              return (n) => {
                let r = e(n);
                if (r.success) return r;
                throw n.parseErrorF(r.expecting, t);
              };
            }),
            (t.named = B),
            (t.nat = void 0),
            (t.notFollowedBy = q),
            (t.ok = h),
            (t.oneOf = I),
            (t.onews = void 0),
            (t.opt = function (e, t) {
              return (n) => {
                n.checkpoint();
                let r = e(n);
                return r.success
                  ? (n.forgetCheckpoint(), h(r.value, r.consumed))
                  : (n.resetCheckpoint(), h(t, !1));
              };
            }),
            (t.pBind = j),
            (t.pBind_ = A),
            (t.pChar = g),
            (t.pChars = function (e) {
              return y(
                (t) => (0, i.some)((e) => e === t, e),
                `<any of '${e}'>`
              );
            }),
            (t.pExactStrings = function (e) {
              let t = I(...e.map((e) => R(e)));
              return (t.parserName = e.join()), t;
            }),
            (t.pNot = D),
            (t.pSplice = void 0),
            (t.pTry = F),
            (t.pTryUnnamed = function (e, t) {
              return F(B(e, t));
            }),
            (t.parens = function (e) {
              return $(e, L("("), L(")"));
            }),
            (t.peekItem = _),
            (t.ret = v),
            (t.sat = y),
            (t.sepList = function (e, t) {
              return (n) => {
                let r = e(n);
                if (!r.success) return r.consumed ? r : h([], !1);
                let o = k(A(t, e))(n);
                return (
                  (o.consumed = r.consumed || o.consumed),
                  o.success && o.value.unshift(r.value),
                  o
                );
              };
            }),
            (t.token = z),
            (t.until = function (e) {
              return T((e) => e.join(""), k(A(D(e), m)));
            }),
            (t.word = L);
          var r,
            o = (function (e, t) {
              if (e && e.__esModule) return e;
              if (
                null === e ||
                ("object" != typeof e && "function" != typeof e)
              )
                return { default: e };
              var n = u(t);
              if (n && n.has(e)) return n.get(e);
              var r = {},
                o = Object.defineProperty && Object.getOwnPropertyDescriptor;
              for (var s in e)
                if (
                  "default" !== s &&
                  Object.prototype.hasOwnProperty.call(e, s)
                ) {
                  var i = o ? Object.getOwnPropertyDescriptor(e, s) : null;
                  i && (i.get || i.set)
                    ? Object.defineProperty(r, s, i)
                    : (r[s] = e[s]);
                }
              return (r.default = e), n && n.set(e, r), r;
            })(n(877)),
            s = (r = n(738)) && r.__esModule ? r : { default: r },
            i = n(268),
            a = n(775);
          function u(e) {
            if ("function" != typeof WeakMap) return null;
            var t = new WeakMap(),
              n = new WeakMap();
            return (u = function (e) {
              return e ? n : t;
            })(e);
          }
          class l {
            constructor(e, t = [], n = new o.Position(0, 1, 0)) {
              (this._input = new s.default(e)),
                (this._context = []),
                (this._splices = t),
                (this.pos = n);
            }
            pushContext(e) {
              this._context.push([e, this.pos.clone()]);
            }
            popContext() {
              this._context.pop();
            }
            checkpoint() {
              this._input.checkpoint(this.pos.clone());
            }
            forgetCheckpoint() {
              this._input.forgetCheckpoint();
            }
            resetCheckpoint() {
              this.pos = this._input.reset();
            }
            peek() {
              return this._input.peek();
            }
            advance() {
              let e = this._input.next();
              return e.done || this.pos.advance(e.value), e.value;
            }
            getSplice(e) {
              return this._splices[e];
            }
            parseErrorF(e, t) {
              let n, r;
              this._context.length > 0 &&
                ([n, r] = this._context[this._context.length - 1]),
                this.checkpoint();
              let s = T(
                (e, t) => e + t.join(""),
                N,
                k(y((e) => !o.isWhitespace(e), ""))
              )(this);
              this.resetCheckpoint();
              let i = "<?>";
              return (
                s.success && (i = s.value),
                new p(this.pos.clone(), e, i, n, r, t)
              );
            }
            parseError(e, t) {
              return this.parseErrorF(() => e, t);
            }
          }
          function c(e, t = []) {
            return new l(e[Symbol.iterator](), t);
          }
          function h(e, t = !0) {
            return { success: !0, value: e, consumed: t };
          }
          function d(e, t = !1) {
            return f(() => e, t);
          }
          function f(e, t = !1) {
            return { success: !1, expecting: e, consumed: t };
          }
          t.ParseState = l;
          class p extends Error {
            constructor(e, t, n, r, o, s) {
              super(),
                (this._pos = e),
                (this._expecting = t),
                (this._unexpected = n),
                (this._ctxName = r),
                (this._ctxPos = o),
                (this._msg = s);
            }
            get name() {
              return "HotDrink Parse Error";
            }
            set name(e) {
              throw Error("Cannot set the name of ParseError");
            }
            get message() {
              let e = this._pos.toString() + "\n";
              null != this._msg && (e += this._msg + "\n");
              let t = this._formatExpecting();
              return (
                "" !== t && (e += "Expecting " + t + ". "),
                (e +=
                  "Unexpected " +
                  (null != this._unexpected ? this._unexpected : "<eof>") +
                  "\n"),
                null != this._ctxName &&
                  null != this._ctxPos &&
                  (e +=
                    "\n  while parsing " +
                    this._ctxName +
                    " (" +
                    this._ctxPos.toString() +
                    ")\n"),
                e
              );
            }
            set message(e) {
              throw Error("Cannot set the message of ParseError");
            }
            _formatExpecting() {
              let e = this._expecting();
              return 1 === e.length
                ? e[0]
                : e.length > 1
                ? "one of (" + e.join(", ") + ")"
                : "";
            }
          }
          function m(e) {
            let t = e.advance();
            return null == t ? d(["<item>"]) : h(t);
          }
          function _(e) {
            let t = e.peek();
            return null == t ? d(["<peek-item>"]) : h(t, !1);
          }
          function v(e, t = !1) {
            return (n) => h(e, t);
          }
          function b(e) {
            return _(e).success ? d(["<eof>"]) : h(null, !1);
          }
          function y(e, t = "<sat: ?>") {
            let n = (n) => {
              let r = _(n);
              return r.success && e(r.value) ? m(n) : d([t]);
            };
            return (n.parserName = t), n;
          }
          function g(e) {
            return y((t) => t === e, e);
          }
          t.ParseError = p;
          const w = y(o.isAlphaPlus, "<letter>");
          t.alphaPlus = w;
          const S = y(o.isAlphaNum, "<letter or digit>");
          t.alphaNum = S;
          const C = y(o.isDigit, "<digit>");
          function k(e) {
            return I(M(e), v([]));
          }
          function M(e) {
            return (t) => T((e, t) => (t.unshift(e), t), e, k(e))(t);
          }
          function R(e) {
            let t = (t) => {
              t.checkpoint();
              for (let n of e) {
                let r = m(t);
                if ((r.success && r.value !== n) || !r.success)
                  return t.resetCheckpoint(), d([e]);
              }
              return t.forgetCheckpoint(), h(e, e.length > 0);
            };
            return (t.parserName = e), t;
          }
          t.digit = C;
          const P = T(
            (e, t, n) => e + t.join("") + n,
            R("//"),
            k(y((e) => "\n" !== e)),
            I(g("\n"), A(b, v("")))
          );
          t.lineComment = P;
          const O = T(
            (e, t, n) => e + t.join("") + n,
            R("/*"),
            k(A(D(R("*/")), m)),
            I(R("*/"), A(b, v("")))
          );
          t.blockComment = O;
          const x = y(o.isWhitespace, "<whitespace>");
          t.onews = x;
          const N = T((e) => e.join(""), k(I(x, P, O)));
          function j(e, t) {
            return (n) => {
              const r = e(n);
              if (!r.success) return r;
              let o = t(r.value)(n);
              return o.consumed || (o.consumed = r.consumed), o;
            };
          }
          function A(e, t) {
            return j(e, (e) => t);
          }
          t.manyws = N;
          const E = {};
          function T(e, ...t) {
            return (n) => {
              let r = [],
                o = !1;
              for (let e of t) {
                let t = e(n);
                if (
                  ((t.consumed = t.consumed || o), (o = t.consumed), !t.success)
                )
                  return t;
                t.value !== E && r.push(t.value);
              }
              return h(e(...r));
            };
          }
          function V(e) {
            return function (t) {
              let n = !1,
                r = e.next();
              for (; !r.done; ) {
                let o = r.value(t);
                if (
                  ((o.consumed = o.consumed || n), (n = o.consumed), !o.success)
                )
                  return o;
                r = e.next(o.value);
              }
              return void 0 === r.value
                ? (a.hdconsole.assert(!1, "Incorrect yield parser"),
                  d(["<?>"], n))
                : h(r.value, n);
            };
          }
          function $(e, t, n) {
            return V(
              (function* () {
                yield t;
                const r = yield e;
                return yield n, r;
              })()
            );
          }
          function I(...e) {
            return (t) => {
              let n = [];
              for (let r of e) {
                let e = r(t);
                if (e.success || e.consumed) return e;
                n.push(e.expecting);
              }
              return f(() => Array.prototype.concat(...n.map((e) => e())));
            };
          }
          function D(e) {
            return (t) => {
              t.checkpoint();
              let n = e(t);
              if (!n.success && !n.consumed)
                return t.resetCheckpoint(), h(null, !1);
              let r = n.consumed;
              return (
                n.success
                  ? (t.resetCheckpoint(), (r = !1))
                  : t.forgetCheckpoint(),
                d(["not(" + e.parserName + ")"], r)
              );
            };
          }
          function q(e, t) {
            return (n) => {
              n.checkpoint();
              let r = e(n);
              if (!r.success) return n.forgetCheckpoint(), r;
              let o = D(t)(n);
              return o.success
                ? (n.forgetCheckpoint(), r)
                : o.consumed
                ? (n.forgetCheckpoint(), o)
                : (n.resetCheckpoint(),
                  d([e.parserName + " not followed by " + t.parserName]));
            };
          }
          function F(e) {
            return (t) => {
              t.checkpoint();
              let n = e(t);
              return n.success
                ? (t.forgetCheckpoint(), n)
                : (t.resetCheckpoint(), d([e.parserName]));
            };
          }
          function B(e, t = "<?>") {
            let n = (n) => {
              let r = e(n);
              return r.success || (r.expecting = () => [t]), r;
            };
            return (n.parserName = t), n;
          }
          function z(e) {
            return T((e, t) => e, e, N);
          }
          function L(e) {
            return z(R(e));
          }
          let U = T(
            (e) => Number(e.join("")),
            B(z(q(B(M(C), "<nat>"), B(w, "<alphaplus>"))), "<nat>")
          );
          (t.nat = U),
            (t.currentPos = (e) => h(e.pos.clone(), !1)),
            (t.pSplice = (e) =>
              j($(U, R("###"), L("###")), (t) => v(e.getSplice(t)))(e));
        },
        738: (e, t) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.default = void 0),
            (t.default = class {
              constructor(e) {
                (this._iter = e),
                  (this._currentIndex = 0),
                  (this._firstIndex = 0),
                  (this._buffer = []),
                  (this._checkpoints = []);
              }
              next() {
                if (0 === this._fetch(1)) return { done: !0 };
                let e = { value: this._at(this._currentIndex++), done: !1 };
                return this._collect(), e;
              }
              _ind2buf(e) {
                return e - this._firstIndex;
              }
              _at(e) {
                return this._buffer[this._ind2buf(e)];
              }
              _fetch(e) {
                let t =
                  e - (this._buffer.length - this._ind2buf(this._currentIndex));
                for (; t > 0; ) {
                  let e = this._iter.next();
                  if (e.done) break;
                  t--, this._buffer.push(e.value);
                }
                return e - t;
              }
              _collect() {
                let e =
                  0 === this._checkpoints.length
                    ? this._currentIndex - this._firstIndex
                    : this._checkpoints[0].index - this._firstIndex;
                (this._firstIndex += e), this._buffer.splice(0, e);
              }
              checkpoint(e) {
                this._checkpoints.push({ index: this._currentIndex, value: e });
              }
              forgetCheckpoint() {
                if (void 0 === this._checkpoints.pop())
                  throw "Trying to forget a nonexisting checkpoint in PeekIterator";
                this._collect();
              }
              reset() {
                if (0 === this._checkpoints.length)
                  throw "Trying to reset to a nonexisting checkpoint in PeekIterator";
                let e = this._checkpoints.pop();
                return (this._currentIndex = e.index), this._collect(), e.value;
              }
              peek() {
                return this._fetch(1), this._at(this._currentIndex);
              }
              peekMany(e) {
                return (
                  this._fetch(e),
                  this._buffer.slice(
                    this._ind2buf(this._currentIndex),
                    this._ind2buf(this._currentIndex + e)
                  )
                );
              }
            });
        },
        269: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.AssignToConstantAction = void 0);
          var r,
            o = n(775),
            s = n(794),
            i = ((r = n(98)) && r.__esModule, n(888));
          function a(e, t, n) {
            if (!t.has(e))
              throw new TypeError(
                "attempted to " + n + " private field on non-instance"
              );
            return t.get(e);
          }
          var u = new WeakMap();
          t.AssignToConstantAction = class {
            constructor(e, t, n) {
              var r, s;
              (function (e, t, n) {
                !(function (e, t) {
                  if (t.has(e))
                    throw new TypeError(
                      "Cannot initialize the same private elements twice on an object"
                    );
                })(e, t),
                  t.set(e, n);
              })(this, u, { writable: !0, value: void 0 }),
                "symbol" == typeof n || "function" == typeof n
                  ? o.hdconsole.warn(
                      "Symbols and functions are not yet supported"
                    )
                  : void 0 === n &&
                    o.hdconsole.warn("Undefined will be converted to null"),
                (s = e),
                (function (e, t, n) {
                  if (t.set) t.set.call(e, n);
                  else {
                    if (!t.writable)
                      throw new TypeError(
                        "attempted to set read only private field"
                      );
                    t.value = n;
                  }
                })((r = this), a(r, u, "set"), s),
                (this._varRef = t),
                (this._newValue = null != n ? n : null);
            }
            toDSL() {
              return `${this._varRef.qualifiedName} = ${JSON.stringify(
                this._newValue
              )};`;
            }
            replayAction() {
              this._varRef.toVariable(this.system).set(this._newValue);
            }
            changeTargetComponent(e, t) {
              this._varRef.compName === t && (this._varRef.compName = e);
            }
            canChangeTargetComponent(e, t) {
              return s.VariableReferenceHolder.canChangeComponent(
                this.system,
                e,
                t,
                this._varRef
              );
            }
            interpretIntent() {
              const e = this._varRef.toVariableNullable(this.system);
              return null == e
                ? [{ recommended: !0, dsl: this.toDSL() }]
                : (0, i.interpretIntent)(e.value, this._newValue, e, !1);
            }
            get system() {
              return (function (e, t) {
                return t.get ? t.get.call(e) : t.value;
              })(this, a(this, u, "get"));
            }
          };
        },
        982: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.AssignToVariableValueAction = void 0);
          var r,
            o = n(43),
            s = ((r = n(98)) && r.__esModule, n(794));
          class i extends o.ScheduleCommandAction {
            constructor(e, t, n) {
              super(e, [n], [t], (e) => e, !1),
                (this._varRef = t),
                (this._varRefIn = n);
            }
            toDSL() {
              return `${this._varRef.qualifiedName} = ${this._varRefIn.qualifiedName};`;
            }
            changeTargetComponent(e, t) {
              this._varRef.compName === t && (this._varRef.compName = e),
                this._varRefIn.compName === t && (this._varRefIn.compName = e);
            }
            canChangeTargetComponent(e, t) {
              return s.VariableReferenceHolder.canChangeComponent(
                this.system,
                e,
                t,
                this._varRef,
                this._varRefIn
              );
            }
            interpretIntent() {
              return [{ recommended: !0, dsl: this.toDSL() }];
            }
          }
          t.AssignToVariableValueAction = i;
        },
        371: (__unused_webpack_module, exports, __webpack_require__) => {
          Object.defineProperty(exports, "__esModule", { value: !0 }),
            (exports.AssignmentOperatorAction = void 0);
          var _scheduleCommandAction = __webpack_require__(43),
            _constraintSystem = _interopRequireDefault(__webpack_require__(98)),
            _variableReferenceHolder = __webpack_require__(794);
          function _interopRequireDefault(e) {
            return e && e.__esModule ? e : { default: e };
          }
          class AssignmentOperatorAction extends _scheduleCommandAction.ScheduleCommandAction {
            constructor(cs, varRef, type, value) {
              super(
                cs,
                [varRef],
                [varRef],
                (v) => eval(`v ${type} ${value}`),
                !1
              ),
                (this._type = type),
                (this._varRef = varRef),
                (this._value = value);
            }
            toDSL() {
              return `${this._varRef.qualifiedName} ${this._type}= ${this._value};`;
            }
            changeTargetComponent(e, t) {
              this._varRef.compName === t && (this._varRef.compName = e);
            }
            canChangeTargetComponent(e, t) {
              return _variableReferenceHolder.VariableReferenceHolder.canChangeComponent(
                this.system,
                e,
                t,
                this._varRef
              );
            }
            interpretIntent() {
              return [{ recommended: !0, dsl: this.toDSL() }];
            }
          }
          exports.AssignmentOperatorAction = AssignmentOperatorAction;
        },
        814: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.componentAddIdentifier = t.ComponentAddAction = void 0),
            s(n(98));
          var r = s(n(894)),
            o = n(986);
          function s(e) {
            return e && e.__esModule ? e : { default: e };
          }
          function i(e, t, n) {
            if (!t.has(e))
              throw new TypeError(
                "attempted to " + n + " private field on non-instance"
              );
            return t.get(e);
          }
          t.componentAddIdentifier = "#AddComponent";
          var a = new WeakMap();
          t.ComponentAddAction = class {
            constructor(e, t) {
              if (
                ((u = { writable: !0, value: void 0 }),
                (function (e, t) {
                  if (t.has(e))
                    throw new TypeError(
                      "Cannot initialize the same private elements twice on an object"
                    );
                })((n = this), (s = a)),
                s.set(n, u),
                (function (e, t, n) {
                  (function (e, t, n) {
                    if (t.set) t.set.call(e, n);
                    else {
                      if (!t.writable)
                        throw new TypeError(
                          "attempted to set read only private field"
                        );
                      t.value = n;
                    }
                  })(e, i(e, t, "set"), n);
                })(this, a, e),
                t instanceof r.default)
              ) {
                const e = Array.from(t._constraints.values())
                  .filter((e) => !e._optional)
                  .map((e) => {
                    var n;
                    return (0, o.constraintToJson)(
                      e,
                      null !== (n = t.constraintName(e)) && void 0 !== n
                        ? n
                        : "<unnamed>"
                    );
                  });
                this._component = (0, o.componentToJson)(t, e);
              } else this._component = t;
              var n, s, u;
            }
            replayAction() {
              let [e, t] = (0, o.componentFromJson)(this._component);
              this.system.addComponent(e), t.forEach((e) => e());
            }
            toDSL() {
              return `#AddComponent ${JSON.stringify(this._component)};`;
            }
            changeTargetComponent(e, t) {
              this._component.name === t && (this._component.name = e);
            }
            canChangeTargetComponent(e, t) {
              return null != this.system.componentByName(e);
            }
            interpretIntent() {
              return [{ recommended: !0, dsl: this.toDSL() }];
            }
            get system() {
              return (function (e, t) {
                return t.get ? t.get.call(e) : t.value;
              })(this, i(this, a, "get"));
            }
          };
        },
        501: (e, t, n) => {
          var r;
          function o(e, t, n) {
            if (!t.has(e))
              throw new TypeError(
                "attempted to " + n + " private field on non-instance"
              );
            return t.get(e);
          }
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.componentRemoveIdentifier = t.ComponentRemoveAction = void 0),
            (r = n(98)) && r.__esModule;
          t.componentRemoveIdentifier = "#RemoveComponent";
          var s = new WeakMap();
          t.ComponentRemoveAction = class {
            constructor(e, t) {
              var n, r;
              (function (e, t, n) {
                !(function (e, t) {
                  if (t.has(e))
                    throw new TypeError(
                      "Cannot initialize the same private elements twice on an object"
                    );
                })(e, t),
                  t.set(e, n);
              })(this, s, { writable: !0, value: void 0 }),
                (r = e),
                (function (e, t, n) {
                  if (t.set) t.set.call(e, n);
                  else {
                    if (!t.writable)
                      throw new TypeError(
                        "attempted to set read only private field"
                      );
                    t.value = n;
                  }
                })((n = this), o(n, s, "set"), r),
                (this._componentName = t);
            }
            replayAction() {
              var e;
              null === (e = this.system.componentByName(this._componentName)) ||
                void 0 === e ||
                e.disconnectSystem();
            }
            toDSL() {
              return `#RemoveComponent ${this._componentName};`;
            }
            changeTargetComponent(e, t) {
              !0 === this.canChangeTargetComponent(e, t) &&
                (this._componentName = e);
            }
            canChangeTargetComponent(e, t) {
              return (
                null != this.system.componentByName(e) &&
                this._componentName === t
              );
            }
            interpretIntent() {
              return [{ recommended: !0, dsl: this.toDSL() }];
            }
            get system() {
              return (function (e, t) {
                return t.get ? t.get.call(e) : t.value;
              })(this, o(this, s, "get"));
            }
          };
        },
        121: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.LinkReferenceAction = void 0);
          var r,
            o = n(794),
            s = ((r = n(98)) && r.__esModule, n(775));
          function i(e, t, n) {
            if (!t.has(e))
              throw new TypeError(
                "attempted to " + n + " private field on non-instance"
              );
            return t.get(e);
          }
          var a = new WeakMap();
          t.LinkReferenceAction = class {
            constructor(e, t, n) {
              var r, o;
              (function (e, t, n) {
                !(function (e, t) {
                  if (t.has(e))
                    throw new TypeError(
                      "Cannot initialize the same private elements twice on an object"
                    );
                })(e, t),
                  t.set(e, n);
              })(this, a, { writable: !0, value: void 0 }),
                (o = e),
                (function (e, t, n) {
                  if (t.set) t.set.call(e, n);
                  else {
                    if (!t.writable)
                      throw new TypeError(
                        "attempted to set read only private field"
                      );
                    t.value = n;
                  }
                })((r = this), i(r, a, "set"), o),
                (this._varRefAssigned = t),
                (this._varRefAssign = n);
            }
            replayAction() {
              if (this._varRefAssigned.equals(this._varRefAssign))
                return void s.hdconsole.warn(
                  "Linking a variable to itself has no effect"
                );
              const e = this._varRefAssigned.toVariableReference(this.system),
                t = this._varRefAssign.toVariableReference(this.system);
              e.link(t);
            }
            toDSL() {
              return `${this._varRefAssigned.qualifiedName} = &${this._varRefAssign.qualifiedName};`;
            }
            changeTargetComponent(e, t) {
              this._varRefAssigned.compName === t &&
                (this._varRefAssigned.compName = e),
                this._varRefAssign.compName === t &&
                  (this._varRefAssign.compName = e);
            }
            canChangeTargetComponent(e, t) {
              return o.VariableReferenceHolder.canChangeComponent(
                this.system,
                e,
                t,
                this._varRefAssigned,
                this._varRefAssign
              );
            }
            interpretIntent() {
              return [{ recommended: !0, dsl: this.toDSL() }];
            }
            get system() {
              return (function (e, t) {
                return t.get ? t.get.call(e) : t.value;
              })(this, i(this, a, "get"));
            }
          };
        },
        43: (__unused_webpack_module, exports, __webpack_require__) => {
          Object.defineProperty(exports, "__esModule", { value: !0 }),
            (exports.ScheduleCommandAction = void 0);
          var _constraintSystem = _interopRequireDefault(
              __webpack_require__(98)
            ),
            _variable = _interopRequireDefault(__webpack_require__(927)),
            _hdconsole = __webpack_require__(775),
            _variableReferenceHolder = __webpack_require__(794);
          function _interopRequireDefault(e) {
            return e && e.__esModule ? e : { default: e };
          }
          function _classPrivateFieldInitSpec(e, t, n) {
            _checkPrivateRedeclaration(e, t), t.set(e, n);
          }
          function _checkPrivateRedeclaration(e, t) {
            if (t.has(e))
              throw new TypeError(
                "Cannot initialize the same private elements twice on an object"
              );
          }
          function _classPrivateFieldGet(e, t) {
            return _classApplyDescriptorGet(
              e,
              _classExtractFieldDescriptor(e, t, "get")
            );
          }
          function _classApplyDescriptorGet(e, t) {
            return t.get ? t.get.call(e) : t.value;
          }
          function _classPrivateFieldSet(e, t, n) {
            return (
              _classApplyDescriptorSet(
                e,
                _classExtractFieldDescriptor(e, t, "set"),
                n
              ),
              n
            );
          }
          function _classExtractFieldDescriptor(e, t, n) {
            if (!t.has(e))
              throw new TypeError(
                "attempted to " + n + " private field on non-instance"
              );
            return t.get(e);
          }
          function _classApplyDescriptorSet(e, t, n) {
            if (t.set) t.set.call(e, n);
            else {
              if (!t.writable)
                throw new TypeError("attempted to set read only private field");
              t.value = n;
            }
          }
          var _cs = new WeakMap();
          class ScheduleCommandAction {
            constructor(cs, ins, outs, func, recordAsCommand) {
              _classPrivateFieldInitSpec(this, _cs, {
                writable: !0,
                value: void 0,
              }),
                _classPrivateFieldSet(this, _cs, cs),
                (this._ins = ins),
                (this._outs = outs),
                (this._recordAsCommand = recordAsCommand),
                "string" == typeof func
                  ? ((this._func = eval(`let f = ${func};\n      f`)),
                    _hdconsole.hdconsole.assert(
                      "function" == typeof this._func,
                      "Eval'd func not function"
                    ),
                    (this._funcStr = func))
                  : "function" == typeof func &&
                    ((this._func = func), (this._funcStr = func.toString()));
            }
            _getVars(e) {
              return Array.from(e, (e) => e.toVariable(this.system));
            }
            replayAction() {
              this.system.scheduleCommand(
                this._getVars(this._ins),
                this._getVars(this._outs),
                this._func,
                this._recordAsCommand
              );
            }
            toDSL() {
              return `(${Array.from(
                this._ins,
                (e) => e.qualifiedName
              ).join()} -> ${Array.from(
                this._outs,
                (e) => e.qualifiedName
              ).join()}) ${this._funcStr};`;
            }
            changeTargetComponent(e, t) {
              function n(n) {
                for (let r of n) r.compName === t && (r.compName = e);
              }
              n(this._ins), n(this._outs);
            }
            canChangeTargetComponent(e, t) {
              return (
                _variableReferenceHolder.VariableReferenceHolder.canChangeComponent(
                  this.system,
                  e,
                  t,
                  ...this._ins
                ) &&
                _variableReferenceHolder.VariableReferenceHolder.canChangeComponent(
                  this.system,
                  e,
                  t,
                  ...this._outs
                )
              );
            }
            interpretIntent() {
              return [{ recommended: !0, dsl: this.toDSL() }];
            }
            get system() {
              return _classPrivateFieldGet(this, _cs);
            }
          }
          exports.ScheduleCommandAction = ScheduleCommandAction;
        },
        477: (e, t, n) => {
          var r;
          function o(e, t, n) {
            !(function (e, t) {
              if (t.has(e))
                throw new TypeError(
                  "Cannot initialize the same private elements twice on an object"
                );
            })(e, t),
              t.set(e, n);
          }
          function s(e, t, n) {
            return (
              t in e
                ? Object.defineProperty(e, t, {
                    value: n,
                    enumerable: !0,
                    configurable: !0,
                    writable: !0,
                  })
                : (e[t] = n),
              e
            );
          }
          function i(e, t) {
            return (function (e, t) {
              return t.get ? t.get.call(e) : t.value;
            })(e, u(e, t, "get"));
          }
          function a(e, t, n) {
            return (
              (function (e, t, n) {
                if (t.set) t.set.call(e, n);
                else {
                  if (!t.writable)
                    throw new TypeError(
                      "attempted to set read only private field"
                    );
                  t.value = n;
                }
              })(e, u(e, t, "set"), n),
              n
            );
          }
          function u(e, t, n) {
            if (!t.has(e))
              throw new TypeError(
                "attempted to " + n + " private field on non-instance"
              );
            return t.get(e);
          }
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.CustomRecordedAction = void 0),
            (r = n(98)) && r.__esModule;
          var l = new WeakMap(),
            c = new WeakMap();
          class h {
            constructor(e, t) {
              if (
                (o(this, l, { writable: !0, value: void 0 }),
                o(this, c, { writable: !0, value: void 0 }),
                !1 === h.IDENTIFIER_REGEX.test(e))
              )
                throw new Error(
                  `Identifier string must match the regex ${h.IDENTIFIER_REGEX.toString()}`
                );
              a(this, l, e), a(this, c, t);
            }
            get identifier() {
              return i(this, l);
            }
            replayAction() {
              return i(this, c).call(this);
            }
            toDSL() {
              return `${h.CUSTOM_TAG} ${i(this, l)};`;
            }
            changeTargetComponent(e, t) {}
            canChangeTargetComponent(e, t) {
              return !1;
            }
            interpretIntent() {
              return [];
            }
          }
          (t.CustomRecordedAction = h),
            s(h, "CUSTOM_TAG", "#custom"),
            s(h, "IDENTIFIER_REGEX", /^[a-zA-Z_$][a-zA-Z_$0-9]*$/);
        },
        888: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.historyOf = l),
            (t.interpretIntent = function (e, t, n, r = !1) {
              const o = [];
              for (const [r, s] of a) r(t) && r(e) && o.push(...s(e, t, n));
              if (!0 === r) return o;
              const s = l(n);
              function i(e) {
                s.clear();
                for (let t of e) s.set(t.dsl, t.recommended);
              }
              if (0 === s.size) return i(o), o;
              const u = [];
              for (const e of o) s.has(e.dsl) && u.push(e);
              if (0 === u.length) return i(o), o;
              for (const e of o) s.has(e.dsl) || (e.recommended = !1);
              return i(u), o;
            }),
            (t.typeIntent = void 0);
          var r,
            o = n(958),
            s = n(284),
            i = n(458);
          (r = n(927)) && r.__esModule;
          const a = new Map();
          (t.typeIntent = a),
            a.set(
              i.constantAssignmentPrecondition,
              i.interpretConstantAssignmentIntent
            ),
            a.set(o.numberPrecondition, o.interpretNumberIntent),
            a.set(
              s.variableAssignmentPrecondition,
              s.interpretVariableAssignmentIntent
            );
          const u = { name: void 0, history: new Map() };
          function l(e) {
            let t = u.name;
            return (
              (u.name = e.owner.qualifiedName),
              t !== e.owner.qualifiedName && u.history.clear(),
              u.history
            );
          }
        },
        458: (e, t, n) => {
          var r;
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.constantAssignmentPrecondition = void 0),
            (t.interpretConstantAssignmentIntent = function (e, t, n) {
              if (typeof e != typeof t) return [];
              function r(e) {
                return [
                  { recommended: !0, dsl: `${n.owner.qualifiedName} = ${e};` },
                ];
              }
              return "string" == typeof t
                ? r(`"${t}"`)
                : "boolean" == typeof t || "number" == typeof t
                ? r(`${t.toString()}`)
                : [];
            }),
            (r = n(927)) && r.__esModule,
            (t.constantAssignmentPrecondition = (e) => {
              const t = typeof e;
              return "string" === t || "boolean" === t || "number" === t;
            });
        },
        958: (e, t, n) => {
          var r;
          function o(e) {
            let t = e.toString(),
              n = t.indexOf(".");
            return (
              Number.isFinite(e) &&
              0 !== e &&
              (-1 === n || t.substr(n + 1).replace(/^0+/, "").length <= 3)
            );
          }
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.interpretNumberIntent = function (e, t, n) {
              const r = [];
              r.push({
                recommended: t > e,
                dsl: `${n.owner.qualifiedName} += ${t - e};`,
              }),
                r.push({
                  recommended: t < e,
                  dsl: `${n.owner.qualifiedName} -= ${e - t};`,
                });
              const s = t / e,
                i = e / t;
              if (
                (r.push({
                  recommended: o(s) && e !== t && s.toString() >= i.toString(),
                  dsl: `${n.owner.qualifiedName} *= ${s};`,
                }),
                r.push({
                  recommended: o(i) && e !== t && s.toString() <= i.toString(),
                  dsl: `${n.owner.qualifiedName} /= ${i};`,
                }),
                t > e &&
                  t === e ** 2 &&
                  r.push({
                    recommended: !0,
                    dsl: `${n.owner.qualifiedName} **= 2;`,
                  }),
                t < e)
              )
                for (let o = 2; o <= 16; o++) {
                  if (e >> o === t) {
                    r.push({
                      recommended: !0,
                      dsl: `${n.owner.qualifiedName} >>= ${o};`,
                    });
                    break;
                  }
                  if (e >> o < t) break;
                }
              else
                for (let o = 2; o <= 16; o++) {
                  if (e << o === t) {
                    r.push({
                      recommended: !0,
                      dsl: `${n.owner.qualifiedName} <<= ${o};`,
                    });
                    break;
                  }
                  if (e << o > t) break;
                }
              return r;
            }),
            (t.numberPrecondition = void 0),
            (r = n(927)) && r.__esModule,
            (t.numberPrecondition = (e) => Number.isFinite(e));
        },
        284: (e, t, n) => {
          var r;
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.interpretVariableAssignmentIntent = function (e, t, n) {
              const r = [];
              for (let e of null !==
                (o =
                  null === (s = n.system) || void 0 === s
                    ? void 0
                    : s.variables()) && void 0 !== o
                ? o
                : []) {
                var o, s;
                e !== n &&
                  e.value === t &&
                  (r.push({
                    recommended: !0,
                    dsl: `${n.owner.qualifiedName} = ${e.owner.qualifiedName};`,
                  }),
                  r.push({
                    recommended: !0,
                    dsl: `${n.owner.qualifiedName} = &${e.owner.qualifiedName};`,
                  }));
              }
              return r;
            }),
            (t.variableAssignmentPrecondition = void 0),
            (r = n(927)) && r.__esModule,
            (t.variableAssignmentPrecondition = (e) => !0);
        },
        211: () => {},
        80: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.buildScriptRecorder = function (e, t, ...n) {
              let r = (0, s.hdl)(
                  t.map((e) => decodeURIComponent(e)),
                  ...n
                ),
                m = (0, s.must)((0, s.pBind_)(s.manyws, x))(r);
              return (
                (0, s.must)(s.eof, "Did you forget the last semicolon?")(r),
                (function (e, t) {
                  let n = new o.ScriptRecorder(!0);
                  const r = t.declarations.map((t) => {
                    switch (t.tag) {
                      case y: {
                        const n = t.value,
                          r = n.varRef.varRefHolder;
                        return new a.AssignToConstantAction(
                          e,
                          r,
                          n.initializer.value
                        );
                      }
                      case g: {
                        const n = t.value,
                          r = n.varRef.varRefHolder;
                        return new c.AssignmentOperatorAction(
                          e,
                          r,
                          n.type,
                          n.initializer.value
                        );
                      }
                      case w: {
                        const n = t.value,
                          r = n.varRef.varRefHolder,
                          o = n.otherVarRef.varRefHolder;
                        return new h.AssignToVariableValueAction(e, r, o);
                      }
                      case S: {
                        const n = t.value,
                          r = n.varRef.varRefHolder,
                          o = n.otherVarRef.varRefHolder;
                        return new l.LinkReferenceAction(e, r, o);
                      }
                      case C: {
                        const n = t.value;
                        return (
                          b ||
                            (p.hdconsole.warn(
                              "Using schedule commands in parsed recorder might not work, the original function variables will be lost. Only use hotdrink variables."
                            ),
                            (b = !0)),
                          new d.ScheduleCommandAction(
                            e,
                            n.inVarRef.map((e) => e.varRefHolder),
                            n.outVarRef.map((e) => e.varRefHolder),
                            n.func.value,
                            !0
                          )
                        );
                      }
                      case k: {
                        const n = t.value;
                        return new f.ComponentAddAction(e, n.componentJson);
                      }
                      case M: {
                        const n = t.value;
                        return new _.ComponentRemoveAction(e, n.componentName);
                      }
                      case R: {
                        const e = t.value,
                          n = o.ScriptRecorder.getCustomAction(e.identifier);
                        return (0, u.throw_on_null)(n);
                      }
                      default:
                        throw new Error(
                          "Internal Error: Unknown tag: " + t.tag
                        );
                    }
                  });
                  return n.record(...r), n.stopRecording(), n;
                })(
                  e,
                  (0, i.asRight)(
                    (function (e, t) {
                      const n = new Set();
                      for (let r of t.declarations)
                        if (r.tag === C) {
                          const t = r.value;
                          for (let r of (0, u.join)([
                            t.inVarRef,
                            t.outVarRef,
                          ])) {
                            let t = E(e, r, n);
                            if ((0, i.isLeft)(t))
                              return (0, i.left)((0, i.asLeft)(t));
                            n.add((0, i.asRight)(t));
                          }
                        } else if (r.tag === k) {
                          const e = r.value,
                            t = e.componentJson.name;
                          for (let r of e.componentJson.variables)
                            n.add(`${t}.${r.name}`);
                        } else if (r.tag === M);
                        else {
                          if (r.tag === R) {
                            const e = r.value;
                            return !1 ===
                              o.ScriptRecorder.hasCustomAction(e.identifier)
                              ? (0, i.left)(
                                  new i.TypeError(
                                    `Unknown custom action with the identifier '${e.identifier}'`,
                                    e.pos
                                  )
                                )
                              : (0, i.right)(t);
                          }
                          {
                            let o = E(e, r.value.varRef, n);
                            if ((0, i.isLeft)(o))
                              return (0, i.left)((0, i.asLeft)(o));
                            switch ((n.add((0, i.asRight)(o)), r.tag)) {
                              case w:
                              case S:
                                o = E(e, r.value.otherVarRef, n);
                                break;
                              case y:
                              case g:
                                continue;
                              default:
                                return (0, i.left)(
                                  new i.TypeError(
                                    "Internal Error: Unknown tag: " + r.tag,
                                    t.pos
                                  )
                                );
                            }
                            if ((0, i.isLeft)(o))
                              return (0, i.left)((0, i.asLeft)(o));
                            n.add((0, i.asRight)(o));
                          }
                        }
                      return (0, i.right)(t);
                    })(e, m.value)
                  )
                )
              );
            });
          var r,
            o = n(806),
            s = n(316),
            i = n(230),
            a = (n(877), (r = n(98)) && r.__esModule, n(269)),
            u = n(268),
            l = n(121),
            c = n(371),
            h = n(982),
            d = n(43),
            f = n(814),
            p = n(775),
            m = n(794),
            _ = n(501),
            v = n(477);
          let b = !1;
          const y = "assign-constant",
            g = "assign-non-constant",
            w = "assign-left-variable-to-right-value",
            S = "assign-left-variable-to-right-variable",
            C = "schedule-command",
            k = "component-add",
            M = "component-remove",
            R = "custom";
          function P(e, t) {
            return { pos: e, declarations: t };
          }
          const O = (0, s.many1)(
            (0, s.oneOf)(
              (0, s.pTryUnnamed)(
                (0, s.pBind)(
                  function (e) {
                    return (0, s.context)(
                      `<${S}>`,
                      (0, s.pBind)(s.currentPos, (e) =>
                        (0, s.lift)(
                          (t, n, r) => ({
                            pos: e,
                            varRef: t,
                            otherVarRef: n,
                            initializer: r,
                          }),
                          N,
                          (0, s.ignore)((0, s.word)("=")),
                          (0, s.ignore)((0, s.word)("&")),
                          j,
                          (0, s.ignore)((0, s.word)(";"))
                        )
                      )
                    )(e);
                  },
                  (e) => (0, s.ret)({ tag: S, value: e })
                ),
                `<${S}>`
              ),
              (0, s.pTryUnnamed)(
                (0, s.pBind)(
                  function (e) {
                    return (0, s.context)(
                      `<${w}>`,
                      (0, s.pBind)(s.currentPos, (e) =>
                        (0, s.lift)(
                          (t, n, r) => ({
                            pos: e,
                            varRef: t,
                            otherVarRef: n,
                            initializer: r,
                          }),
                          j,
                          (0, s.ignore)((0, s.word)("=")),
                          j,
                          (0, s.ignore)((0, s.word)(";"))
                        )
                      )
                    )(e);
                  },
                  (e) => (0, s.ret)({ tag: w, value: e })
                ),
                `<${w}>`
              ),
              (0, s.pTryUnnamed)(
                (0, s.pBind)(
                  function (e) {
                    return (0, s.context)(
                      "<assign-non-constant>",
                      (0, s.pBind)(s.currentPos, (e) =>
                        (0, s.lift)(
                          (t, n, r) => ({
                            pos: e,
                            varRef: t,
                            type: n,
                            initializer: r,
                          }),
                          j,
                          (0, s.ignore)(s.manyws),
                          (0, s.oneOf)(
                            (0, s.pExactStrings)([
                              "||",
                              "&&",
                              ">>>",
                              ">>",
                              "<<",
                              "**",
                              "??",
                            ]),
                            (0, s.pChars)("-+*/|&%^")
                          ),
                          (0, i.withPos)(A),
                          (0, s.ignore)(s.manyws)
                        )
                      )
                    )(e);
                  },
                  (e) => (0, s.ret)({ tag: g, value: e })
                ),
                "<assign-non-constant>"
              ),
              (0, s.pTryUnnamed)(
                (0, s.pBind)(
                  function (e) {
                    return (0, s.context)(
                      "<assign-constant>",
                      (0, s.pBind)(s.currentPos, (e) =>
                        (0, s.lift)(
                          (t, n) => ({ pos: e, varRef: t, initializer: n }),
                          j,
                          (0, i.withPos)(A),
                          (0, s.ignore)(s.manyws)
                        )
                      )
                    )(e);
                  },
                  (e) => (0, s.ret)({ tag: y, value: e })
                ),
                "<assign-constant>"
              ),
              (0, s.pTryUnnamed)(
                (0, s.pBind)(
                  function (e) {
                    return (0, s.context)(
                      "<schedule-command>",
                      (0, s.pBind)(s.currentPos, (e) =>
                        (0, s.lift)(
                          (t, n, r) => ({
                            pos: e,
                            inVarRef: t,
                            outVarRef: n,
                            func: r,
                          }),
                          (0, s.ignore)((0, s.word)("(")),
                          (0, s.sepList)(j, (0, s.word)(",")),
                          (0, s.ignore)((0, s.word)("->")),
                          (0, s.sepList)(j, (0, s.word)(",")),
                          (0, s.ignore)((0, s.word)(")")),
                          (0, i.withPos)((0, i.jsCode)([";"], !1)),
                          (0, s.ignore)((0, s.word)(";"))
                        )
                      )
                    )(e);
                  },
                  (e) => (0, s.ret)({ tag: C, value: e })
                ),
                "<schedule-command>"
              ),
              (0, s.pTryUnnamed)(
                (0, s.pBind)(
                  function (e) {
                    return (0, s.context)(
                      "<component-add>",
                      (0, s.pBind)(s.currentPos, (e) =>
                        (0, s.lift)(
                          (t) => ({ pos: e, componentJson: JSON.parse(t) }),
                          (0, s.inBetween)(
                            (0, i.jsCode)([";"], !1),
                            (0, s.word)(f.componentAddIdentifier),
                            (0, s.word)(";")
                          )
                        )
                      )
                    )(e);
                  },
                  (e) => (0, s.ret)({ tag: k, value: e })
                ),
                "<component-add>"
              ),
              (0, s.pTryUnnamed)(
                (0, s.pBind)(
                  function (e) {
                    return (0, s.context)(
                      "<component-remove>",
                      (0, s.pBind)(s.currentPos, (e) =>
                        (0, s.lift)(
                          (t) => ({ pos: e, componentName: t }),
                          (0, s.inBetween)(
                            s.identifier,
                            (0, s.word)(_.componentRemoveIdentifier),
                            (0, s.word)(";")
                          )
                        )
                      )
                    )(e);
                  },
                  (e) => (0, s.ret)({ tag: M, value: e })
                ),
                "<component-remove>"
              ),
              (0, s.pTryUnnamed)(
                (0, s.pBind)(
                  function (e) {
                    return (0, s.context)(
                      "<custom>",
                      (0, s.pBind)(s.currentPos, (e) =>
                        (0, s.lift)(
                          (t) => ({ pos: e, identifier: t }),
                          (0, s.inBetween)(
                            s.identifier,
                            (0, s.word)(v.CustomRecordedAction.CUSTOM_TAG),
                            (0, s.word)(";")
                          )
                        )
                      )
                    )(e);
                  },
                  (e) => (0, s.ret)({ tag: R, value: e })
                ),
                "<custom>"
              )
            )
          );
          function x(e) {
            return (0, s.lift)(P, s.currentPos, O)(e);
          }
          function N(e) {
            return j(e, !1);
          }
          function j(e, t = !0) {
            return (0, s.context)(
              "<var-ref>",
              (0, s.pBind)(s.currentPos, (e) =>
                (0, s.lift)(
                  (n, r) => ({
                    pos: e,
                    varRefHolder: new m.VariableReferenceHolder(n, r, t),
                  }),
                  s.identifier,
                  (0, s.ignore)((0, s.exactString)(".")),
                  s.identifier
                )
              )
            )(e);
          }
          function A(e) {
            return (0, s.pBind_)(
              (0, s.word)("="),
              (0, s.context)(
                "<intitializer-expression>",
                (0, s.pBind)((0, i.jsCode)([";"], !0), (e) =>
                  (0, s.ret)(new Function("return " + e)())
                )
              )
            )(e);
          }
          function E(e, t, n) {
            const r = t.varRefHolder,
              o = r.qualifiedName,
              s = r.toVariableReferenceNullable(e);
            if (!n.has(o)) {
              if (null == e.componentByName(r.compName))
                return (0, i.left)(
                  new i.TypeError(`Unknown component '${r.compName}'`, t.pos)
                );
              if (null == s)
                return (0, i.left)(
                  new i.TypeError(
                    `Unknown variable reference '${r.varName}' in (valid) component '${r.compName}'`,
                    t.pos
                  )
                );
            }
            return r.mustHaveValue && null != s && null == s.value && !n.has(o)
              ? (0, i.left)(
                  new i.TypeError(
                    `The variable reference specified cannot be dangling. Variable '${r.varName}' in component '${r.compName}'`,
                    t.pos
                  )
                )
              : (0, i.right)(o);
          }
        },
        806: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.ScriptRecorder = void 0);
          var r = (function (e, t) {
              if (e && e.__esModule) return e;
              if (
                null === e ||
                ("object" != typeof e && "function" != typeof e)
              )
                return { default: e };
              var n = s(t);
              if (n && n.has(e)) return n.get(e);
              var r = {},
                o = Object.defineProperty && Object.getOwnPropertyDescriptor;
              for (var i in e)
                if (
                  "default" !== i &&
                  Object.prototype.hasOwnProperty.call(e, i)
                ) {
                  var a = o ? Object.getOwnPropertyDescriptor(e, i) : null;
                  a && (a.get || a.set)
                    ? Object.defineProperty(r, i, a)
                    : (r[i] = e[i]);
                }
              return (r.default = e), n && n.set(e, r), r;
            })(n(534)),
            o = n(775);
          function s(e) {
            if ("function" != typeof WeakMap) return null;
            var t = new WeakMap(),
              n = new WeakMap();
            return (s = function (e) {
              return e ? n : t;
            })(e);
          }
          function i(e, t, n) {
            !(function (e, t) {
              if (t.has(e))
                throw new TypeError(
                  "Cannot initialize the same private elements twice on an object"
                );
            })(e, t),
              t.set(e, n);
          }
          function a(e, t) {
            return h(e, l(e, t, "get"));
          }
          function u(e, t, n) {
            return (
              (function (e, t, n) {
                if (t.set) t.set.call(e, n);
                else {
                  if (!t.writable)
                    throw new TypeError(
                      "attempted to set read only private field"
                    );
                  t.value = n;
                }
              })(e, l(e, t, "set"), n),
              n
            );
          }
          function l(e, t, n) {
            if (!t.has(e))
              throw new TypeError(
                "attempted to " + n + " private field on non-instance"
              );
            return t.get(e);
          }
          function c(e, t, n) {
            return (
              (function (e, t) {
                if (e !== t)
                  throw new TypeError(
                    "Private static access of wrong provenance"
                  );
              })(e, t),
              (function (e, t) {
                if (void 0 === e)
                  throw new TypeError(
                    "attempted to get private static field before its declaration"
                  );
              })(n),
              h(e, n)
            );
          }
          function h(e, t) {
            return t.get ? t.get.call(e) : t.value;
          }
          n(477);
          var d = new WeakMap(),
            f = new WeakMap();
          class p {
            static setCustomAction(e) {
              c(p, p, m).set(e.identifier, e);
            }
            static getCustomAction(e) {
              var t;
              return null !== (t = c(p, p, m).get(e)) && void 0 !== t
                ? t
                : null;
            }
            static hasCustomAction(e) {
              const t = "string" == typeof e ? e : e.identifier;
              return c(p, p, m).has(t);
            }
            static removeCustomAction(e) {
              const t = "string" == typeof e ? e : e.identifier;
              return c(p, p, m).delete(t);
            }
            static allCustomActions() {
              return [...c(p, p, m).values()];
            }
            async playCustomAction(e) {
              const t = p.getCustomAction(e);
              if (null == t)
                return void o.hdconsole.warn(`Unknown custom action '${e}'`);
              const n = this.recording;
              this.stopRecording(),
                await t.replayAction(),
                !0 === n && (this.startRecording(), this.record(t));
            }
            constructor(e = !0) {
              var t, n;
              (n = []),
                (t = "_recordedActions") in this
                  ? Object.defineProperty(this, t, {
                      value: n,
                      enumerable: !0,
                      configurable: !0,
                      writable: !0,
                    })
                  : (this[t] = n),
                i(this, d, { writable: !0, value: void 0 }),
                i(this, f, { writable: !0, value: void 0 }),
                u(this, d, e),
                u(this, f, new r.Subject());
            }
            get recording() {
              return a(this, d);
            }
            get size() {
              return this._recordedActions.length;
            }
            subscribe(e) {
              return a(this, f).subscribe(e);
            }
            toDSL() {
              return encodeURIComponent(
                this._recordedActions.map((e) => e.toDSL()).join("")
              );
            }
            startRecording() {
              u(this, d, !0);
            }
            stopRecording() {
              return u(this, d, !1), this;
            }
            async replay() {
              if (0 === this.size) return;
              const e = { recorder: this, cancel: !1 };
              if ((a(this, f).sendNext(e), !0 === e.cancel)) return;
              const t = this.recording;
              this.stopRecording();
              for (const e of this._recordedActions) await e.replayAction();
              !0 === t && this.startRecording();
            }
            record(...e) {
              if (this.recording)
                for (let t of e) {
                  const e = { recorder: this, action: t, cancel: !1 };
                  a(this, f).sendNext(e),
                    !0 !== e.cancel && this._recordedActions.push(t);
                }
            }
            merge(e) {
              const t = { recorder: this, otherRecorder: e, cancel: !1 };
              a(this, f).sendNext(t),
                !0 !== t.cancel &&
                  this._recordedActions.push(...e._recordedActions);
            }
            changeTargetComponent(e, t) {
              for (const n of this._recordedActions)
                if (!0 !== n.canChangeTargetComponent(e, t)) return !1;
              const n = {
                recorder: this,
                cancel: !1,
                newTarget: e,
                oldTarget: t,
              };
              if ((a(this, f).sendNext(n), !0 === n.cancel)) return !1;
              for (const n of this._recordedActions)
                n.changeTargetComponent(e, t);
              return !0;
            }
          }
          t.ScriptRecorder = p;
          var m = { writable: !0, value: new Map() };
        },
        794: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.VariableReferenceHolder = void 0),
            s(n(98));
          var r = s(n(831)),
            o = n(292);
          function s(e) {
            return e && e.__esModule ? e : { default: e };
          }
          s(n(927));
          class i {
            static from(e, t = !1) {
              if (!(e instanceof r.default))
                throw new Error(
                  "Can only get a VariableReferenceHolder from a VariableReference"
                );
              if (!e.owner.hasVariableReferenceName(e))
                throw new Error(
                  "Cannot create variable reference holder of a variable reference before it has been added to the owners references"
                );
              return new i(e.owner.name, e.name, t);
            }
            static canChangeComponent(e, t, n, ...r) {
              for (const s of r) {
                var o;
                if (
                  s.compName === n &&
                  null ==
                    (null === (o = e.componentByName(t)) || void 0 === o
                      ? void 0
                      : o.getVariableReference(s.varName))
                )
                  return !1;
              }
              return !0;
            }
            constructor(e, t, n) {
              (this.compName = e), (this.varName = t), (this.mustHaveValue = n);
            }
            toVariableReference(e) {
              const t = this.toVariableReferenceNullable(e);
              if (null == t)
                throw null == e.componentByName(this.compName)
                  ? new TypeError(`Unknown component '${this.compName}'`)
                  : new TypeError(
                      `Unknown variable reference '${this.varName}' in (valid) component '${this.compName}'`
                    );
              return t;
            }
            toVariableReferenceNullable(e) {
              var t;
              return null === (t = e.componentByName(this.compName)) ||
                void 0 === t
                ? void 0
                : t.vs[this.varName];
            }
            toVariable(e) {
              return (0, o.variableFromReference)(this.toVariableReference(e));
            }
            toVariableNullable(e) {
              const t = this.toVariableReferenceNullable(e);
              return null == t ? null : (0, o.variableFromReference)(t);
            }
            get qualifiedName() {
              return `${this.compName}.${this.varName}`;
            }
            equals(e) {
              return e instanceof i && this.qualifiedName === e.qualifiedName;
            }
          }
          t.VariableReferenceHolder = i;
        },
        268: (e, t, n) => {
          Object.defineProperty(t, "__esModule", { value: !0 }),
            (t.RefListNode =
              t.RefList =
              t.PriorityList =
              t.OneToManyMap =
              t.ObservableReference =
              t.ListNode =
              t.LinkedList =
              t.BinaryHeap =
                void 0),
            (t.adjacentFind = m),
            (t.bisame = v),
            (t.bisimilar = function (e, t) {
              return b((e, t) => i(e, t), e, t);
            }),
            (t.clear = function (e) {
              e.length = 0;
            }),
            (t.copy = function e(t) {
              return (function (t) {
                if (Array.isArray(t)) return t.map((t) => e(t));
                if (t instanceof Set) return new Set(t);
                if (t instanceof Map)
                  return new Map(p(([t, n]) => [t, e(n)], t));
                if ("object" == typeof t && null !== t) {
                  if ("function" == typeof t.copy) return t.copy();
                  {
                    let n = Object.create(Object.getPrototypeOf(t));
                    for (let r of Object.keys(t)) n[r] = e(t[r]);
                    return n;
                  }
                }
                return "function" == typeof t && "function" == typeof t.copy
                  ? t.copy()
                  : t;
              })(t);
            }),
            (t.count = function (e) {
              let t = 0;
              for (let n of e) ++t;
              return t;
            }),
            (t.equals = i),
            (t.every = function (e, t) {
              for (let n of t) if (!e(n)) return !1;
              return !0;
            }),
            (t.every2 = b),
            (t.filter = function (e, t) {
              return {
                [Symbol.iterator]: () => {
                  const n = t[Symbol.iterator]();
                  let r = {
                    next: () => {
                      let t,
                        r = !1;
                      do {
                        ({ done: r, value: t } = n.next());
                      } while (!r && !e(t));
                      return { done: r, value: t };
                    },
                  };
                  return (
                    "function" == typeof n.return &&
                      (r.return = (e) => n.return(e)),
                    "function" == typeof n.throw &&
                      (r.throw = (e) => n.throw(e)),
                    r
                  );
                },
              };
            }),
            (t.first = function (e) {
              const t = e[Symbol.iterator]();
              let n = t.next();
              return n.done
                ? void 0
                : ("function" == typeof t.return && t.return(), n.value);
            }),
            (t.firstKeepOpen = function (e) {
              let t = e[Symbol.iterator]().next();
              return t.done ? void 0 : t.value;
            }),
            (t.foldl = function (e, t, n) {
              let r = t;
              for (let t of n) r = e(t, r);
              return r;
            }),
            (t.forEach = function (e, t) {
              for (let n of t) e(n);
              return e;
            }),
            (t.isSortedBy = function (e, t) {
              return null === m((t, n) => !e(t, n), t);
            }),
            (t.isUnique = function (e) {
              return 1 === C(e);
            }),
            (t.iterableToIterator = f),
            (t.iteratorToIterable = d),
            (t.join = function (e) {
              return {
                [Symbol.iterator]: () => {
                  let t,
                    n = f(e),
                    r = !0,
                    o = !1;
                  return {
                    next: () => {
                      if (o) return { done: !0 };
                      let e, s;
                      for (;;) {
                        if (!0 === r) {
                          if (((e = n.next()), e.done)) return { done: !0 };
                          t = f(e.value);
                        }
                        if (((s = t.next()), !s.done))
                          return (r = !1), { done: !1, value: s.value };
                        r = !0;
                      }
                    },
                    return: (e) => (
                      o ||
                        ((o = !0),
                        r || "function" != typeof t.return || t.return(e)),
                      { done: !0, value: e }
                    ),
                    throw: (e) => {
                      if (o) throw e;
                    },
                  };
                },
              };
            }),
            (t.map = p),
            (t.mapEquals = function (e, t) {
              if (e.size !== t.size) return !1;
              for (let [n, r] of e) if (!i(t.get(n), r)) return !1;
              return !0;
            }),
            (t.mkEmptyIterable = h),
            (t.mkEmptyIterator = function () {
              return { next: () => ({ done: !0, value: void 0 }) };
            }),
            (t.mkRefCounted = function (e, t, n) {
              return (
                r.hdconsole.assert(
                  0 === C(e),
                  "trying to make an object reference counted twice"
                ),
                g.set(e, t),
                null != n && w.set(e, n),
                y.set(e, 1),
                e
              );
            }),
            (t.mkSingletonIterable = function* (e) {
              yield e;
            }),
            (t.modifyMapValue = l),
            (t.nonnull_cast = function (e) {
              return e;
            }),
            (t.orderedMapEquals = u),
            (t.orderedSetEquals = a),
            (t.refCount = C),
            (t.release = S),
            (t.rest = function (e) {
              let t = f(e);
              return t.next(), d(t);
            }),
            (t.retain = function (e) {
              let t = y.get(e);
              return (
                null == t
                  ? (r.hdconsole.assert(
                      null != t,
                      "trying to retain a non-refcounted object"
                    ),
                    y.set(e, NaN))
                  : y.set(e, t + 1),
                e
              );
            }),
            (t.sameElements = function (e, t) {
              let n = new Map();
              for (let t of e) l(n, t, (e) => (null == e ? 1 : e + 1));
              for (let e of t) {
                let t = n.get(e);
                if (1 === t) n.delete(e);
                else {
                  if (void 0 === t) return !1;
                  n.set(e, t - 1);
                }
              }
              return 0 === n.size;
            }),
            (t.setDifference = function (e, t) {
              let n = new Set();
              for (let r of e) t.has(r) || n.add(r);
              return n;
            }),
            (t.setDifferenceTo = function (e, t) {
              for (let n of t) e.delete(n);
            }),
            (t.setEquals = function (e, t) {
              if (e.size !== t.size) return !1;
              for (let n of e.keys()) if (!t.has(n)) return !1;
              return !0;
            }),
            (t.setUnion = function (e, t) {
              let n = new Set();
              return c(n, e), c(n, t), n;
            }),
            (t.setUnionTo = c),
            (t.setUnionToWithDiff = function (e, t) {
              let n = new Set();
              for (let r of t) e.has(r) || (e.add(r), n.add(r));
              return n;
            }),
            (t.setUniqueHandler = function (e, t) {
              w.set(e, t);
            }),
            (t.some = function (e, t) {
              for (let n of t) if (e(n)) return !0;
              return !1;
            }),
            (t.subsetOf = function (e, t) {
              for (let n of e.keys()) if (!t.has(n)) return !1;
              return !0;
            }),
            (t.throw_on_null = function (e) {
              if (null == e) throw "Expected non-null";
              return e;
            }),
            (t.until = function (e, t) {
              return {
                [Symbol.iterator]: () => {
                  const n = t[Symbol.iterator]();
                  let r = {},
                    o = () => n.next();
                  return (
                    (r.done = !1),
                    (r.next = () => {
                      if (r.done) return { done: !0 };
                      let { done: t, value: s } = o();
                      return (
                        (o = () => n.next()),
                        t
                          ? ((r.done = !0), { done: !0 })
                          : ((t = e(s)),
                            t
                              ? ("function" == typeof n.return && n.return(),
                                { done: !0 })
                              : { done: !1, value: s })
                      );
                    }),
                    "function" == typeof n.return &&
                      (r.return = (e) => n.return(e)),
                    "function" == typeof n.throw &&
                      (r.throw = (e) => ((o = () => n.throw(e)), r.next())),
                    r
                  );
                },
              };
            }),
            (t.zip = function (e, t) {
              return _((e, t) => [e, t], e, t);
            }),
            (t.zipWith = _);
          var r = n(775),
            o = (function (e, t) {
              if (e && e.__esModule) return e;
              if (
                null === e ||
                ("object" != typeof e && "function" != typeof e)
              )
                return { default: e };
              var n = s(t);
              if (n && n.has(e)) return n.get(e);
              var r = {},
                o = Object.defineProperty && Object.getOwnPropertyDescriptor;
              for (var i in e)
                if (
                  "default" !== i &&
                  Object.prototype.hasOwnProperty.call(e, i)
                ) {
                  var a = o ? Object.getOwnPropertyDescriptor(e, i) : null;
                  a && (a.get || a.set)
                    ? Object.defineProperty(r, i, a)
                    : (r[i] = e[i]);
                }
              return (r.default = e), n && n.set(e, r), r;
            })(n(534));
          function s(e) {
            if ("function" != typeof WeakMap) return null;
            var t = new WeakMap(),
              n = new WeakMap();
            return (s = function (e) {
              return e ? n : t;
            })(e);
          }
          function i(e, t) {
            const n = e,
              r = t;
            if (n === r) return !0;
            if (Array.isArray(n) && Array.isArray(r)) {
              if (n.length !== r.length) return !1;
              for (let e = 0; e < n.length; ++e) if (!i(n[e], r[e])) return !1;
              return !0;
            }
            if (n instanceof Set && r instanceof Set) return a(n, r);
            if (n instanceof Map && r instanceof Map) return u(n, r);
            if ("object" == typeof n && null !== n) {
              if ("function" == typeof n.equals) return n.equals(r);
              if (null != r) {
                let e = Object.keys(n),
                  t = Object.keys(r);
                if (e.length !== t.length) return !1;
                for (let t of e) if (!i(n[t], r[t])) return !1;
                return !0;
              }
            }
            return (
              "function" == typeof n &&
              "function" == typeof n.equals &&
              n.equals(r)
            );
          }
          function a(e, t) {
            return e.size === t.size && v(e, t);
          }
          function u(e, t) {
            return (
              e.size === t.size &&
              b(([e, t], [n, r]) => e === n && i(t, r), e, t)
            );
          }
          function l(e, t, n) {
            let r = n(e.get(t));
            return e.set(t, r), r;
          }
          function c(e, t) {
            for (let n of t) e.add(n);
          }
          function* h() {}
          function d(e) {
            let t = {};
            return (
              (t[Symbol.iterator] = function () {
                return e;
              }),
              t
            );
          }
          function f(e) {
            return e[Symbol.iterator]();
          }
          function p(e, t) {
            return {
              [Symbol.iterator]: () => {
                const n = t[Symbol.iterator]();
                let r = {
                  next: () => {
                    const { done: t, value: r } = n.next();
                    return { done: t, value: t ? void 0 : e(r) };
                  },
                };
                return (
                  "function" == typeof n.return &&
                    (r.return = (e) => n.return(e)),
                  "function" == typeof n.throw && (r.throw = (e) => n.throw(e)),
                  r
                );
              },
            };
          }
          function m(e, t) {
            let n = f(t),
              r = n.next();
            if (r.done) return null;
            let o = r.value;
            for (;;) {
              let t = n.next();
              if (t.done) return null;
              if (e(o, t.value))
                return (
                  "function" == typeof n.return && n.return(), [o, t.value]
                );
              o = t.value;
            }
          }
          function _(e, t, n) {
            let r = !1;
            return {
              [Symbol.iterator]: () => {
                const o = t[Symbol.iterator](),
                  s = n[Symbol.iterator]();
                return {
                  next: () => {
                    if (r) return { done: !0 };
                    const t = o.next(),
                      n = s.next();
                    return (
                      (r = t.done || n.done),
                      t.done || "function" != typeof o.return || o.return(),
                      n.done || "function" != typeof s.return || s.return(),
                      r
                        ? { done: !0 }
                        : { done: !1, value: e(t.value, n.value) }
                    );
                  },
                  return: (e) => (
                    "function" == typeof o.return && o.return(),
                    "function" == typeof s.return && s.return(),
                    (r = !0),
                    { done: !0, value: e }
                  ),
                };
              },
            };
          }
          function v(e, t) {
            return b((e, t) => e === t, e, t);
          }
          function b(e, t, n) {
            const r = t[Symbol.iterator](),
              o = n[Symbol.iterator]();
            for (;;) {
              const t = r.next(),
                n = o.next();
              if (!t.done && !n.done) {
                if (e(t.value, n.value)) continue;
                return !1;
              }
              if (t.done && n.done) return !0;
              if (!t.done)
                return "function" == typeof r.return && r.return(), !1;
              if (!n.done)
                return "function" == typeof o.return && o.return(), !1;
            }
            return !0;
          }
          t.OneToManyMap = class {
            constructor() {
              this._m = new Map();
            }
            hasKey(e) {
              return this._m.has(e);
            }
            keys() {
              return this._m.keys();
            }
            values(e) {
              let t = this._m.get(e);
              return void 0 !== t ? t.values() : h();
            }
            countKeys() {
              return this._m.size;
            }
            count(e) {
              let t = this._m.get(e);
              return void 0 === t ? 0 : t.size;
            }
            addKey(e) {
              return (
                void 0 === this._m.get(e) && this._m.set(e, new Set()), this
              );
            }
            add(e, t) {
              let n = this._m.get(e);
              return (
                void 0 === n && ((n = new Set()), this._m.set(e, n)),
                n.add(t),
                this
              );
            }
            addMany(e, t) {
              let n = this._m.get(e);
              void 0 === n && ((n = new Set()), this._m.set(e, n));
              for (let e of t) n.add(e);
            }
            remove(e, t) {
              let n = this._m.get(e);
              return void 0 !== n && n.delete(t);
            }
            removeKey(e) {
              this._m.delete(e);
            }
            clear() {
              this._m.clear();
            }
          };
          let y = new WeakMap(),
            g = new WeakMap(),
            w = new WeakMap();
          function S(e) {
            let t = y.get(e);
            if (
              (r.hdconsole.assert(
                null != t && t > 0,
                "trying to release an already released object"
              ),
              --t,
              y.set(e, t),
              t > 1)
            )
              return t;
            if (1 === t) {
              let t = w.get(e);
              null != t && t(e);
            } else {
              let t = g.get(e);
              null != t && t(e);
            }
            return t;
          }
          function C(e) {
            let t = y.get(e);
            return void 0 === t ? 0 : t;
          }
          t.RefList = class {
            constructor() {
              this._head = null;
            }
            front() {
              if (null != this._head) return this._head.data;
            }
            pushFront(e) {
              return (this._head = new k(this._head, e)), this._head;
            }
          };
          class k {
            constructor(e, t) {
              r.hdconsole.assert(C(t) > 0), (this.next = e), (this.data = t);
            }
            releaseTail() {
              let e = this.next;
              for (; null != e; ) S(e.data), (e = e.next);
              this.next = null;
            }
          }
          t.RefListNode = k;
          class M {
            constructor(e, t, n) {
              (this.prev = e), (this.next = t), (this.data = n);
            }
          }
          t.ListNode = M;
          class R {
            constructor() {
              (this._head = null), (this._tail = null), (this._size = 0);
            }
            insertNode(e, t) {
              return (
                null == e
                  ? ((t.prev = this._tail), (t.next = null), (this._tail = t))
                  : ((t.prev = e.prev), (t.next = e), (e.prev = t)),
                null == t.prev ? (this._head = t) : (t.prev.next = t),
                this._size++,
                t
              );
            }
            insert(e, t) {
              return this.insertNode(e, new M(null, null, t));
            }
            pushFront(e) {
              return this.insert(this._head, e);
            }
            pushNodeFront(e) {
              return this.insertNode(this._head, e);
            }
            pushBack(e) {
              return this.insert(null, e);
            }
            pushNodeBack(e) {
              return this.insertNode(null, e);
            }
            removeNode(e) {
              return (
                null == e.next ? (this._tail = e.prev) : (e.next.prev = e.prev),
                null == e.prev ? (this._head = e.next) : (e.prev.next = e.next),
                this._size--,
                e
              );
            }
            remove(e) {
              return this.removeNode(e).data;
            }
            popFront() {
              return null == this._head ? null : this.remove(this._head);
            }
            popBack() {
              return null == this._tail ? null : this.remove(this._tail);
            }
            [Symbol.iterator]() {
              let e = this._head;
              return {
                next: () => {
                  if (null == e) return { done: !0 };
                  {
                    let t = { value: e.data, done: !1 };
                    return (e = e.next), t;
                  }
                },
              };
            }
            reverseIterator() {
              let e = this._tail;
              return {
                next: () => {
                  if (null == e) return { done: !0 };
                  {
                    let t = { value: e.data, done: !1 };
                    return (e = e.prev), t;
                  }
                },
              };
            }
          }
          t.LinkedList = R;
          class P {
            size() {
              return this._list._size;
            }
            constructor() {
              (this._nodeMap = new Map()),
                (this._list = new R()),
                (this._highestPriority = -1),
                (this._lowestPriority = 0);
            }
            _access(e) {
              let t = this._nodeMap.get(e);
              if (null == t) throw "element not found in priority list";
              return t;
            }
            remove(e) {
              let t = this._nodeMap.get(e);
              null != t && (this._nodeMap.delete(e), this._list.remove(t));
            }
            promote(e) {
              let t = this._access(e);
              (t.data.priority = ++this._highestPriority),
                this._list.pushNodeFront(this._list.removeNode(t));
            }
            demote(e) {
              let t = this._access(e);
              (t.data.priority = --this._lowestPriority),
                this._list.pushNodeBack(this._list.removeNode(t));
            }
            get highestPriority() {
              return this._highestPriority;
            }
            get lowestPriority() {
              return this._lowestPriority;
            }
            priority(e) {
              return this._access(e).data.priority;
            }
            pushBack(e) {
              this._nodeMap.set(
                e,
                this._list.pushBack({
                  data: e,
                  priority: --this._lowestPriority,
                })
              );
            }
            pushFront(e) {
              this._nodeMap.set(
                e,
                this._list.pushFront({
                  data: e,
                  priority: ++this._highestPriority,
                })
              );
            }
            entries() {
              return p((e) => e.data, this);
            }
            [Symbol.iterator]() {
              return this._list[Symbol.iterator]();
            }
            hasHigherPriority(e, t) {
              return (
                this._access(e).data.priority > this._access(t).data.priority
              );
            }
          }
          t.PriorityList = P;
          var O = new WeakSet();
          function x() {
            null != this._subscription &&
              (this._subscription.unsubscribe(), (this._subscription = null));
          }
          (t.ObservableReference = class {
            constructor(e = null) {
              var t, n;
              (function (e, t) {
                if (t.has(e))
                  throw new TypeError(
                    "Cannot initialize the same private elements twice on an object"
                  );
              })((t = this), (n = O)),
                n.add(t),
                (this._value = e),
                (this._subject = new o.Subject((e) => {
                  e.next(this._value);
                })),
                (this._subscription = null);
            }
            subscribe(e) {
              return this._subject.subscribe(e);
            }
            get value() {
              return this._value;
            }
            set value(e) {
              (function (e, t, n) {
                if (!t.has(e))
                  throw new TypeError(
                    "attempted to get private field on non-instance"
                  );
                return n;
              })(this, O, x).call(this),
                this._setValue(e);
            }
            _setValue(e) {
              null !== this._value && this._subject.sendNext(null),
                (this._value = e),
                null !== e && this._subject.sendNext(e);
            }
            link(e) {
              const t = this._value;
              (this.value = null),
                (this._subscription = e.subscribe((e) => {
                  this._setValue(e);
                })),
                null != t &&
                  !0 === t.hasOwnProperty("_subject") &&
                  "function" ==
                    typeof (null == t ? void 0 : t._subject).sendComplete &&
                  (null == t || t._subject.sendComplete());
            }
          }),
            (t.BinaryHeap = class {
              constructor(e) {
                (this._cmp = e), (this._data = [null]);
              }
              size() {
                return this._data.length - 1;
              }
              empty() {
                return 0 === this.size();
              }
              _swap(e, t) {
                let n = this._data[e];
                (this._data[e] = this._data[t]), (this._data[t] = n);
              }
              _percolateUp(e) {
                let t = e;
                for (; t >= 2; ) {
                  let e = Math.floor(t / 2);
                  if (!this._cmp(this._data[t], this._data[e])) break;
                  this._swap(t, e), (t = e);
                }
              }
              push(e) {
                this._data.push(e), this._percolateUp(this.size());
              }
              _percolateDown(e) {
                let t = e;
                for (; 2 * t <= this.size(); ) {
                  let e = this._minOfSiblings(2 * t);
                  if (this._cmp(this._data[t], this._data[e])) break;
                  this._swap(t, e), (t = e);
                }
              }
              _minOfSiblings(e) {
                return e >= this.size() ||
                  this._cmp(this._data[e], this._data[e + 1])
                  ? e
                  : e + 1;
              }
              pop() {
                let e = this._data[1],
                  t = this._data.pop();
                return (
                  this.size() > 0 &&
                    ((this._data[1] = t), this._percolateDown(1)),
                  e
                );
              }
            });
        },
      },
      __webpack_module_cache__ = {};
    function __webpack_require__(e) {
      var t = __webpack_module_cache__[e];
      if (void 0 !== t) return t.exports;
      var n = (__webpack_module_cache__[e] = { exports: {} });
      return (
        __webpack_modules__[e](n, n.exports, __webpack_require__), n.exports
      );
    }
    var __webpack_exports__ = __webpack_require__(707);
    return __webpack_exports__;
  })();
});
//# sourceMappingURL=hotdrink.js.map
