const COLUMNS = 3;
const ROWS = 3;

const system = hd.defaultConstraintSystem;

const HD_ROW_COL_SEP_CHAR = "_";
const ELEM_ROW_COL_SEP_CHAR = ",";
const HD_ROW_COL_PREFIX = "v";
const VARS_COMP_NAME = "vars";
const REF_REGEX_STRING = / *\(?([0-9]+,[0-9]+)\)? */.source;

const LITERAL_MATH_REGEX = new RegExp(
  `^=${REF_REGEX_STRING}([+\\-*/%^&|]) *([0-9]+(\.[0-9]+)?)`
);
const PLAIN_REF_REGEX = /^= *([0-9]+,[0-9]+)$/;

async function parseInput(cellElem, cellElemInput, cellElemDiv) {
  const hdid = hdId(cellElemDiv);
  let oldComp = system.componentByName(hdid);
  if (oldComp != null) {
    console.log("[", cellElem.id, "] disconnecting", hdid);
    oldComp.disconnectSystem();
  }
  const inputValue = cellElemInput.value; // must be before setting input to hidden

  cellElemInput.type = "hidden";
  cellElemDiv.hidden = false;

  let literal;
  if (inputValue !== "" && inputValue[0] === "=") {
    //dynamic, must update this as a reference when the referenced cell is also updated
    //If the binding fails then set as literal
    literal = bindConstraint(inputValue, cellElemDiv);
  } else {
    // Not a reference to another cell, always literal
    literal = true;
  }
  if (literal === true) {
    let hdVarl = hdVar(hdid);
    if (hdVarl.value !== inputValue) {
      hdVarl.set(inputValue);
    }
    await hdVarl.currentPromise;
  }
}

async function createCell(varsComp, rowElem, row, col) {
  const hdVarId = hdVarRefId(row, col);
  //Create a variable in the vars component
  const varRef = varsComp.emplaceVariable(hdVarId, "");
  const cellId = cellElemId(row, col);

  const cellElem = document.createElement("td");
  const cellElemInput = document.createElement("input");
  const cellElemDiv = document.createElement("span");

  hd.variableListener(varRef, (val) => {
    //Update input for convenience, if there is one
    const comp = system.componentByName(hdVarId);

    //Update the input for scripting to be seamless
    if (comp == null) {
      cellElemInput.value = val;
    } else {
      cellElemInput.value = comp.edit.code;
    }

    const inputValue = cellElemInput.value;
    if ((`${val}` === "NaN" || `${val}` === "") && inputValue[0] === "=") {
      cellElemDiv.innerHTML = `<em style="color: #333">${inputValue}</em>`;
    } else if (`${val}` === "" && inputValue === "") {
      cellElemDiv.innerHTML = `<em style="color: gray">(${cellId})</em>`;
    } else {
      cellElemDiv.innerText = val;
    }
  });

  rowElem.appendChild(cellElem);

  cellElem.id = cellElemId(row, col);
  cellElemDiv.id = getCellDivId(cellElem.id);
  cellElemInput.id = getCellInputId(cellElem.id);

  cellElem.onclick = () => toggleInput(cellElem, cellElemInput, cellElemDiv);
  cellElem.appendChild(cellElemInput);
  cellElem.appendChild(cellElemDiv);

  cellElemInput.className = "inputField";
  cellElemInput.onblur = () => parseInput(cellElem, cellElemInput, cellElemDiv);

  await parseInput(cellElem, cellElemInput, cellElemDiv);
}

async function createColumn(varsComp, rows, col) {
  for (let row = 0; row < rows; row++) {
    const rowElem = document.getElementById("row-" + row);
    await createCell(varsComp, rowElem, row, col);
  }
}

async function createRow(varsComp, row, cols) {
  const rowElem = document.createElement("tr");
  rowElem.id = `row-${row}`;
  for (let col = 0; col < cols; col++) {
    await createCell(varsComp, rowElem, row, col);
  }
  document.getElementById("tbody").appendChild(rowElem);
}

async function createCells(varsComp) {
  for (let row = 0; row < ROWS; row++) {
    await createRow(varsComp, row, COLUMNS);
  }
}

function toggleInput(cellElem, cellElemInput, cellElemDiv) {
  //display only the input as the child
  cellElemDiv.hidden = true;
  cellElemInput.type = "text";
  cellElemInput.focus();
}

/**
 * @param inputValue
 * @param outRefElem
 * @return {boolean} If the binding failed
 */
async function bindConstraint(inputValue, outRefElem) {
  let outId = hdId(outRefElem);
  let inId;
  let rightSide;

  if (PLAIN_REF_REGEX.test(inputValue) === true) {
    let refedCellId = PLAIN_REF_REGEX.exec(inputValue)[1];

    let cellDivElement = getCellDivElement(refedCellId);
    if (cellDivElement == null) {
      console.log("Failed to find div elem", refedCellId);
      return true;
    }
    inId = hdId(cellDivElement);

    rightSide = inId.toString();
  } else if (LITERAL_MATH_REGEX.test(inputValue) === true) {
    let [_, refedCellId, op, literal] = LITERAL_MATH_REGEX.exec(inputValue);

    let cellDivElement = getCellDivElement(refedCellId);
    if (cellDivElement == null) {
      console.log("Failed to find div elem", refedCellId);
      return true;
    }
    inId = hdId(cellDivElement);
    rightSide = `Number.parseFloat(${inId}) ${op} Number.parseFloat(${literal})`;
  } else {
    //Failed to match anything
    console.debug("Failed to match anything");
    return true;
  }

  if (inId === outId) {
    console.debug("Referred to self");
    return true;
  }

  // language=textmate
  let hdstr = `
        component ${outId} {
            var &${inId}, &${outId}, code;
            
            constraint {
                (${inId} -> ${outId}) => ${rightSide};
            }
        }`;

  console.log(hdstr);
  let component = hd.component([hdstr]);

  component.edit.code = inputValue;
  component.vs[inId].value = hdVar(inId);
  component.vs[outId].value = hdVar(outId);

  //Must wait for the code to be completely updated before adding it
  await component.vs["code"].currentPromise;
  await hdVar(outId).currentPromise;
  await hdVar(inId).currentPromise;

  system.addComponent(component);

  //the in variable will be dirty, but not automatically updated
  try {
    system.updateDirty();
    return false;
  } catch (e) {
    //Update failed, might be a circular issue
    component.disconnectSystem();
    return true;
  }
}

function getCellDivId(cell_id) {
  return cell_id + "_div";
}

function getCellInputId(cell_id) {
  return cell_id + "_input";
}

function getCellDivElement(cell_id) {
  return document.getElementById(getCellDivId(cell_id));
}

function getCellInputElement(cell_id) {
  return document.getElementById(getCellInputId(cell_id));
}

function id(div_elem) {
  return div_elem.parentElement.id.replace(
    ELEM_ROW_COL_SEP_CHAR,
    HD_ROW_COL_SEP_CHAR
  );
}

function hdId(div_elem) {
  return HD_ROW_COL_PREFIX + id(div_elem);
}

function hdVarRef(id) {
  return system.edit[VARS_COMP_NAME][`${id}_self`];
}

function hdVar(id) {
  return hdVarRef(id).value;
}

function cellElemId(row, col) {
  return `${row}${ELEM_ROW_COL_SEP_CHAR}${col}`;
}

function hdVarRefId(row, col) {
  return `${HD_ROW_COL_PREFIX}${row}${HD_ROW_COL_SEP_CHAR}${col}`;
}

window.onload = async () => {
  //A component without any constraints which holds all variables in the system for consistent referring
  // language=textmate
  let varsComp = hd.component([`component ${VARS_COMP_NAME} {}`]);
  system.addComponent(varsComp);
  system.updateDirty();

  await createCells(varsComp);
  system.updateDirty();

  system.recorder.subscribe((event) => {
    // console.log(event);
    if (event.otherRecorder !== undefined) {
      //Remove all components other than the vars component
      for (let comp of system.allComponents()) {
        if (comp.name === VARS_COMP_NAME) {
          for (let variable of comp._vars) {
            variable.set("");
          }
        } else {
          system.removeComponent(comp);
        }
      }
    }
  });

  //We only want to begin recording after all the static components are created
  system.recorder.startRecording();

  let createRowFn = async () => {
    await createRow(varsComp, row++, column);
    system.updateDirty();
  };

  let createColumnFn = async () => {
    await createColumn(varsComp, row, column++);
    system.updateDirty();
  };

  hd.ScriptRecorder.setCustomAction(
    new hd.CustomRecordedAction("create_row", createRowFn)
  );
  hd.ScriptRecorder.setCustomAction(
    new hd.CustomRecordedAction("create_column", createColumnFn)
  );

  // const customAction = {
  //   replayAction: async () => {
  //     await createRowFn();
  //     await createColumnFn();
  //     document.getElementById("bad_button").onclick();
  //   },
  //   toDSL: () => "#custom create_row_col",
  //   changeTargetComponent: (newTarget, oldTarget) => {},
  //   canChangeTargetComponent: (newTarget, oldTarget) => false,
  //   interpretIntent: () => [],
  // };
  // system.recorder._recordedActions.push(customAction);

  let row = ROWS;
  let column = COLUMNS;

  document.getElementById("createRow").onclick = () => {
    system.recorder.playCustomAction("create_row");
  };
  document.getElementById("createCol").onclick = () => {
    system.recorder.playCustomAction("create_column");
  };
};
