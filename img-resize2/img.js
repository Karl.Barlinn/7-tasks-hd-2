import { numberBinder, onHDChange } from "../packages/binders.js";

function startRecording() {
  let startElement = document.getElementById("start");
  let stopElement = document.getElementById("stop");
  startElement.disabled = true;
  stopElement.disabled = false;

  cs.recorder.startRecording();
}

function stopRecording() {
  let startElement = document.getElementById("start");
  let stopElement = document.getElementById("stop");
  startElement.disabled = false;
  stopElement.disabled = true;
  cs.recorder.stopRecording();
}

async function replay() {
  cs.recorder.replay();
  await cs.update();
}

function updateDSLText() {
  let dslElement = document.getElementById("dsl");
  dslElement.value = decodeURIComponent(cs.recorder.toDSL())
    .split(";")
    .join(";\n");
}

function parseDSLText() {
  resetRecorder();
  let dslElement = document.getElementById("dsl");
  cs.parseRecorderDSL(dslElement.value, false, true);
}

function updateTarget() {
  let oldT = document.getElementById("oldTarget");
  let newT = document.getElementById("newTarget");
  let success = cs.recorder.changeTargetComponent(newT.value, oldT.value);
  if (success === true) {
    let orgOldT = oldT.value;
    oldT.value = newT.value;
    newT.value = orgOldT;
  }
}

function resetRecorder() {
  cs.resetRecorder();
  oldRecSub?.unsubscribe();
  oldRecSub = cs.recorder.subscribe((recordActionEvent) => {
    if (recordActionEvent.action != null) {
      console.log("recordActionEvent", recordActionEvent);
      recordActionEvent.cancel = true;
      const intents = recordActionEvent.action.interpretIntent();

      const getoverlay = () => document.getElementById("hd-script-dsl-overlay");
      const overlay = getoverlay();
      if (overlay == null) {
        return;
      }

      console.log(intents);
      if (intents.length === 0) {
        return;
      }

      const overlayWindow = document.getElementById("hd-script-overlay_window");

      //Remove all children
      while (overlayWindow.lastChild) {
        overlayWindow.removeChild(overlayWindow.lastChild);
      }

      for (let intent of intents) {
        const button = document.createElement("button");
        button.innerText = intent.dsl;
        button.title = intent.explanation;
        button.style.color = intent.recommended ? "#000" : "#777";
        button.onclick = () => {
          cs.parseRecorderDSL(intent.dsl, false, true);
          getoverlay().hidden = true;
        };

        button.style.display = "block";
        button.style.padding = ".1em";
        button.style.marginBottom = ".3em";
        button.style.width = "100%";

        overlayWindow.appendChild(button);
      }

      const button = document.createElement("button");
      button.innerText = "Hide";
      button.onclick = () => (getoverlay().hidden = true);
      button.style.marginTop = "1em";
      button.style.width = "100%";
      overlayWindow.appendChild(button);
      overlay.hidden = false;
    }
  });
}

const cs = hd.defaultConstraintSystem;
let oldRecSub = null;

window.onload = async () => {
  document.getElementById("start").onclick = startRecording;
  document.getElementById("stop").onclick = stopRecording;
  document.getElementById("replay").onclick = replay;
  document.getElementById("update-dsl").onclick = updateDSLText;
  document.getElementById("parse-dsl").onclick = parseDSLText;
  document.getElementById("updateTarget").onclick = updateTarget;

  async function createConstraint(compName, imgElemId, absXElemId, absYElemId) {
    //language=textmate
    const comp = hd.component([
      `
  component ${compName} {
    var relX = 1, relY = 1;
    var width, height;

    constraint {
      (width, height -> relX, relY) => { return [Number.isFinite(width / height) ? width / height : 0, Number.isFinite(height / width) ? height / width : 0]; }
      (relX, relY, width, height -> width, height) => { return [width * relX, height * relY]; }
    }
  }
`,
    ]);

    const imgElem = () => document.getElementById(imgElemId);
    cs.addComponent(comp);

    cs.scheduleCommand([], [comp.vs.width.value, comp.vs.height.value], () => [
      imgElem().width,
      imgElem().height,
    ]);

    numberBinder(document.getElementById(absXElemId), comp.vs.width);
    numberBinder(document.getElementById(absYElemId), comp.vs.height);
    // numberBinder(document.getElementById("rel-x"), comp.vs.relX);
    // numberBinder(document.getElementById("rel-y"), comp.vs.relY);

    await comp.vs.width.value.currentPromise;
    await comp.vs.height.value.currentPromise;

    onHDChange(comp.vs.width, (val) => (imgElem().width = val));
    onHDChange(comp.vs.height, (val) => (imgElem().height = val));
  }

  await createConstraint("image2", "img", "abs-x", "abs-y");
  await createConstraint("image1", "img2", "abs-x2", "abs-y2");

  cs.update(false);

  await hd.defaultConstraintSystem.componentByName("image1").vs.width.value
    .currentPromise;

  resetRecorder();
  startRecording();
};
