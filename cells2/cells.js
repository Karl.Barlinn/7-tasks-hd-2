import { evaluate, Field, lex, parse } from "./parser.js";
import { bindConstraint } from "./constraints.js";

const COLUMNS = 26;
const ROWS = 100;
const START_CHAR = 65;

function createColums() {
  for (let i = -1; i < COLUMNS; i++) {
    let td = document.createElement("td");
    if (i >= 0) {
      td.innerText = String.fromCharCode(i + START_CHAR);
    }
    document.getElementById("thead").appendChild(td);
  }
}

function createCells() {
  for (let i = 0; i < ROWS; i++) {
    let t_row = document.createElement("tr");
    t_row.innerText = i.toString();
    for (let j = 0; j < COLUMNS; j++) {
      let t_cell = document.createElement("td");
      let t_cell_input = document.createElement("input");
      let t_cell_div = document.createElement("div");

      t_row.appendChild(t_cell);

      t_cell.appendChild(t_cell_input);
      t_cell.appendChild(t_cell_div);
      t_cell.id = String.fromCharCode(j + START_CHAR) + i;
      t_cell.onclick = () => toggleInput(t_cell, t_cell_input, t_cell_div);

      t_cell_input.className = "inputField";
      t_cell_input.onblur = () => parseInput(t_cell, t_cell_input, t_cell_div);
      t_cell_input.onchange = () =>
        parseInput(t_cell, t_cell_input, t_cell_div);

      t_cell_div.id = t_cell.id + "_div";

      parseInput(t_cell, t_cell_input, t_cell_div);
    }
    document.getElementById("tbody").appendChild(t_row);
  }
}

function toggleInput(t_cell, t_cell_input, t_cell_div) {
  t_cell_div.style.visibility = "hidden";
  t_cell_input.type = "text";
  t_cell_input.focus();
}

function parseInput(t_cell, t_cell_input, t_cell_div) {
  if (t_cell_input.type !== "hidden") {
    if (t_cell_input.value[0] === "=") {
      let ast = parse(lex(t_cell_input.value.slice(1)));
      if (ast.type === Field) {
        bindConstraint(ast.val, t_cell_div);
      } else {
        evaluate(ast, t_cell_div);
      }
    } else t_cell_div.innerText = t_cell_input.value;

    //only show what is in the input element
    t_cell_input.type = "hidden";
    t_cell_div.style.visibility = "visible";
  }
}

window.onload = () => {
  hd.defaultConstraintSystem.recorder.startRecording();
  createColums();
  createCells();
};
