import { disabledBinder } from "../packages/binders.js";
import { comp } from "./comp.hd.js";

let system = hd.defaultConstraintSystem;
const canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");

let selectedCircleIndex;
let mouseX, mouseY;

let dummy;

let adjust = document.getElementById("adjust");
let slider = document.getElementById("slider");
let undo = document.getElementById("undo");
let redo = document.getElementById("redo");

function history(undo) {
  let vals = [comp.vs.circles.value, comp.vs.history.value];
  system.scheduleCommand(vals, vals, (circles, history) => {
    if (undo) {
      if (dummy !== undefined) {
        adjust.style.display = "none";
        circles[dummy.ref].visible = true;
        dummy = undefined;
      } else {
        let pop = circles.pop();
        if (pop.ref !== undefined) {
          circles[pop.ref].visible = true;
        }
        history.push(pop);
      }
    } else {
      let pop = history.pop();
      if (pop.ref !== undefined && circles[pop.ref] !== undefined) {
        circles[pop.ref].visible = false;
      }
      circles.push(pop);
    }
    return [circles, history];
  });
}

function tick() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  comp.vs.circles.value?.value?.forEach((circle) => {
    if (circle.visible) {
      let path = circle.path;
      if (!(path instanceof Path2D)) {
        path = new Path2D();
        path.arc(circle.x, circle.y, circle.radius, 0, 2 * Math.PI);
        circle.path = path;
        // console.log("new path ", circle.x, circle.y, circle.radius);
      }
      console.assert(
        path instanceof Path2D,
        "Path is STILL not a Path2D ",
        path
      );
      if (ctx.isPointInPath(path, mouseX, mouseY)) {
        ctx.fillStyle = "green";
      } else {
        ctx.fillStyle = "grey";
      }
      ctx.fill(path);
    }
  });
  if (dummy) {
    ctx.fillStyle = "grey";
    ctx.fill(dummy.path);
  }
  window.requestAnimationFrame(tick);
}

function saveSlide() {
  system.scheduleCommand(
    [comp.vs.circles.value],
    [comp.vs.circles.value, comp.vs.history.value],
    (circles) => {
      let obj = circles[selectedCircleIndex];
      obj.visible = false;

      dummy = undefined;

      let newPath = new Path2D();
      newPath.arc(obj.x, obj.y, slider.value, 0, 2 * Math.PI);

      circles.push({
        x: obj.x,
        y: obj.y,
        radius: slider.value,
        path: null,
        visible: true,
        ref: selectedCircleIndex,
      });

      circles[selectedCircleIndex] = obj;
      return [circles, []];
    }
  );
}

let span = document.getElementsByClassName("close")[0];
span.onclick = saveAndCloseSlider;

function saveAndCloseSlider() {
  adjust.style.display = "none";
  saveSlide();
}

window.onload = () => {
  comp.connectSystem(system, false);
  system.update();
  window.requestAnimationFrame(tick);

  disabledBinder(undo, comp.vs.undoDisabled);
  disabledBinder(redo, comp.vs.redoDisabled);

  undo.addEventListener("click", () => history(true));
  redo.addEventListener("click", () => history(false));

  function sliderInputEvent() {
    system.scheduleCommand(
      [comp.vs.circles.value],
      [comp.vs.circles.value],
      (circles) => {
        let obj = circles[selectedCircleIndex];
        obj.visible = false;
        circles[selectedCircleIndex] = obj;

        let newPath = new Path2D();
        newPath.arc(obj.x, obj.y, slider.value, 0, 2 * Math.PI);

        dummy = {
          x: obj.x,
          y: obj.y,
          radius: slider.value,
          path: newPath,
          visible: true,
          ref: selectedCircleIndex,
        };
        return circles;
      }
    );
  }

  slider.addEventListener("input", sliderInputEvent);
  canvas.addEventListener("click", (event) => {
    if (dummy !== undefined) {
      saveAndCloseSlider();
      return;
    }
    for (const circle of comp.vs.circles.value.value) {
      if (ctx.isPointInPath(circle.path, mouseX, mouseY) && circle.visible) {
        // Placement of modal
        adjust.style.display = "block";
        adjust.style.top = circle.y + "px";
        adjust.style.left = circle.x + "px";

        slider.value = circle.radius;
        selectedCircleIndex = comp.vs.circles.value.value.indexOf(circle);
        sliderInputEvent(); //Fixes cannot undo slider unless slider changed bug
        return;
      }
    }
    //Not clicking inside a circle
    let path = new Path2D();
    path.arc(event.x - 10, event.y - 100, 40, 0, 2 * Math.PI);

    system.scheduleCommand(
      [comp.vs.circles.value],
      [comp.vs.circles.value, comp.vs.history.value],
      (circles) => {
        circles.push({
          x: event.x - 10,
          y: event.y - 100,
          radius: 40,
          path: null,
          visible: true,
        });
        return [circles, []];
      }
    );
  });

  canvas.addEventListener("mousemove", function (event) {
    mouseX = event.offsetX;
    mouseY = event.offsetY;
  });
};
