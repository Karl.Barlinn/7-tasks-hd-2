import { numberBinder, onHDChange } from "../packages/binders.js";
import { comp } from "./comp.hd.js";

function startRecording() {
  let startElement = document.getElementById("start");
  let stopElement = document.getElementById("stop");
  startElement.disabled = true;
  stopElement.disabled = false;

  cs.recorder.startRecording();
}

function stopRecording() {
  let startElement = document.getElementById("start");
  let stopElement = document.getElementById("stop");
  startElement.disabled = false;
  stopElement.disabled = true;
  cs.recorder.stopRecording();
}

async function replay() {
  cs.recorder.replay();
  await cs.update();
  updateCurrDSLText();
}

function updateDSLText() {
  let dslElement = document.getElementById("dsl");
  dslElement.value = decodeURIComponent(cs.recorder.toDSL())
    .split(";")
    .join(";\n");
  updateCurrDSLText();
}

function updateCurrDSLText() {
  let dslArea = document.getElementById("dsl-curr");
  dslArea.value =
    "//Current recorder as script\n" +
    decodeURIComponent(cs.recorder.toDSL()).split(";").join(";\n");
}

function parseDSLText() {
  resetRecorder();
  let dslElement = document.getElementById("dsl");
  cs.parseRecorderDSL(dslElement.value, false, true);
  updateCurrDSLText();
}

function resetRecorder() {
  cs.resetRecorder();
  oldRecSub?.unsubscribe();
  oldRecSub = cs.recorder.subscribe((recordActionEvent) => {
    console.log("recordActionEvent", recordActionEvent);
    if (intentEnabled() === true && recordActionEvent.action != null) {
      recordActionEvent.cancel = true;
      const intents = recordActionEvent.action.interpretIntent();

      const getoverlay = () => document.getElementById("hd-script-dsl-overlay");
      const overlay = getoverlay();
      if (overlay == null) {
        return;
      }

      console.log(intents);
      if (intents.length === 0) {
        return;
      }

      const overlayWindow = document.getElementById("hd-script-overlay_window");

      //Remove all children
      while (overlayWindow.lastChild) {
        overlayWindow.removeChild(overlayWindow.lastChild);
      }

      for (let intent of intents) {
        const button = document.createElement("button");
        button.innerText = intent.dsl;
        button.title = intent.explanation;
        button.style.color = intent.recommended ? "#000" : "#777";
        button.onclick = () => {
          cs.parseRecorderDSL(intent.dsl, false, true);
          getoverlay().hidden = true;
        };

        button.style.display = "block";
        button.style.padding = ".1em";
        button.style.marginBottom = ".3em";
        button.style.width = "100%";

        overlayWindow.appendChild(button);
      }

      const button = document.createElement("button");
      button.innerText = "Hide";
      button.onclick = () => (getoverlay().hidden = true);
      button.style.marginTop = "1em";
      button.style.width = "100%";
      overlayWindow.appendChild(button);
      overlay.hidden = false;
    }
    updateCurrDSLText();
  });
}

const cs = hd.defaultConstraintSystem;
let oldRecSub = null;
let intentEnabled = () => document.getElementById("intent-checkbox").checked;

window.onload = async () => {
  const imgElem = document.getElementById("img");

  document.getElementById("start").onclick = startRecording;
  document.getElementById("stop").onclick = stopRecording;
  document.getElementById("replay").onclick = replay;
  document.getElementById("update-dsl").onclick = updateDSLText;
  document.getElementById("parse-dsl").onclick = parseDSLText;

  cs.addComponent(comp);
  cs.scheduleCommand(
    [],
    [
      comp.vs.width.value,
      comp.vs.height.value,
      comp.vs.initWidth.value,
      comp.vs.initHeight.value,
    ],
    () => [imgElem.width, imgElem.height, imgElem.width, imgElem.height]
  );

  stopRecording();
  cs.resetRecorder();
  cs.update(false);

  numberBinder(document.getElementById("init-x"), comp.vs.initWidth);
  numberBinder(document.getElementById("init-y"), comp.vs.initHeight);
  numberBinder(document.getElementById("abs-x"), comp.vs.width);
  numberBinder(document.getElementById("abs-y"), comp.vs.height);
  numberBinder(document.getElementById("rel-x"), comp.vs.relX);
  // comp.vs.relX.value.subscribe((a) => console.log("sub", a));
  // comp.vs.relX.value.subscribeValue((a) => console.log("value", a));
  // comp.vs.relX.value.subscribePending((a) => console.log("pending", a));
  // comp.vs.relX.value.subscribeError((a) => console.log("err", a));
  numberBinder(document.getElementById("rel-y"), comp.vs.relY);

  await comp.vs.width.value.currentPromise;
  await comp.vs.height.value.currentPromise;

  // hd.ScriptRecorder.setCustomAction(
  //   new hd.CustomRecordedAction("test", () => {
  //     console.log("custom action");
  //   })
  // );
  // hd.typeIntent.set(
  //   () => true,
  //   () => {
  //     return [
  //       {
  //         recommended: true,
  //         dsl: "#custom test;",
  //         explanation: "Execute the test custom function",
  //       },
  //     ];
  //   }
  // );

  onHDChange(comp.vs.width, (val) => (imgElem.width = val));
  onHDChange(comp.vs.height, (val) => (imgElem.height = val));
  startRecording();
  resetRecorder();

  setInterval(updateCurrDSLText, 100);

  updateCurrDSLText();
};
