export const comp = hd.component`
  component image {
    var relX = 1, relY = 1;
    var width, height, initWidth, initHeight;

    constraint {
      (relX, relY, initWidth, initHeight -> width, height) => { return [initWidth * relX, initHeight * relY]; }
      (width, height, initWidth, initHeight -> relX, relY) => { return [Number.isFinite(width / initHeight) ? width / initHeight : 0, Number.isFinite(height / initWidth) ?  height / initWidth : 0]; }
    }
  }
`;
