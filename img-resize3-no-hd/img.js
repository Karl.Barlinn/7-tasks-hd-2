////////////////////
// Recorder start //
////////////////////

export const funcs = [];
export let recording = true;

export function startRecording() {
  recording = true;
  document.getElementById("start").disabled = true;
  document.getElementById("stop").disabled = false;
}

export function stopRecording() {
  recording = false;
  document.getElementById("start").disabled = false;
  document.getElementById("stop").disabled = true;
}

export function replay() {
  let oldRec = recording;
  stopRecording();
  for (let action of funcs) {
    action.func(...action.args);
  }
  updateImg();
  recording = oldRec;
}

export function record(func, ...args) {
  if (recording) {
    funcs.push({ func, args });
  }
  func(...args);
  updateImg();
}

//////////////////
// Resize logic //
//////////////////

export function updateImg() {
  const imgElem = document.getElementById("img");
  imgElem.width = getAbsWidth();
  imgElem.height = getAbsHeight();
}

////////////////
// Initiation //
////////////////

window.onload = () => {
  document.getElementById("start").onclick = startRecording;
  document.getElementById("stop").onclick = stopRecording;
  document.getElementById("replay").onclick = replay;

  const imgElem = document.getElementById("img");

  setInitWidth(imgElem.width);
  setInitHeight(imgElem.height);

  setAbsWidth(imgElem.width);
  setAbsHeight(imgElem.height);

  setRelWidth(1);
  setRelHeight(1);

  document.getElementById("rel-x").onchange = () => {
    record((relX) => {
      setRelWidth(relX);
      setAbsWidth(getInitWidth() * relX);
    }, getRelWidth());
  };
  document.getElementById("rel-y").onchange = () => {
    record((relY) => {
      setRelHeight(relY);
      setAbsHeight(getInitHeight() * relY);
    }, getRelHeight());
  };
  document.getElementById("abs-x").onchange = () => {
    record((absX) => {
      setAbsWidth(absX);
      setRelWidth(absX / getInitHeight());
    }, getAbsWidth());
  };
  document.getElementById("abs-y").onchange = () => {
    record((absY) => {
      setAbsHeight(absY);
      setRelHeight(absY / getInitWidth());
    }, getAbsHeight());
  };

  // state aware modification

  document.getElementById("double-width").onclick = () => {
    record(() => {
      const relX = 2 * getRelWidth();
      const absX = 2 * getAbsWidth();
      setAbsWidth(getInitWidth() * relX);
      1;
      setRelWidth(absX / getInitHeight());
    });
  };

  //debug var
  window.funcs = funcs;

  startRecording();
};

/////////////////////////
// getters and setters //
/////////////////////////

export function getRelWidth() {
  return document.getElementById("rel-x").valueAsNumber;
}

export function getRelHeight() {
  return document.getElementById("rel-y").valueAsNumber;
}

export function setRelWidth(num) {
  document.getElementById("rel-x").valueAsNumber = num;
}

export function setRelHeight(num) {
  document.getElementById("rel-y").valueAsNumber = num;
}

export function getAbsWidth() {
  return document.getElementById("abs-x").valueAsNumber;
}

export function getAbsHeight() {
  return document.getElementById("abs-y").valueAsNumber;
}

export function setAbsWidth(num) {
  document.getElementById("abs-x").valueAsNumber = num;
}

export function setAbsHeight(num) {
  document.getElementById("abs-y").valueAsNumber = num;
}

export function getInitWidth() {
  return document.getElementById("init-x").valueAsNumber;
}

export function getInitHeight() {
  return document.getElementById("init-y").valueAsNumber;
}

export function setInitWidth(num) {
  document.getElementById("init-x").valueAsNumber = num;
}

export function setInitHeight(num) {
  document.getElementById("init-y").valueAsNumber = num;
}
