export const comp = hd.component`
  component whap {
      var area = 100, width, height, perimeter;
      constraint perimeter {
        (width, height -> perimeter) => 2 * (width + height);
        (perimeter, width -> height) => perimeter / 2 - width;
        (perimeter, height -> width) => perimeter / 2 - height;
      }
      constraint area {
        (width, height -> area) => width * height;
        (area -> width, height) => [Math.sqrt(area), Math.sqrt(area)];
      }
    }
`;
