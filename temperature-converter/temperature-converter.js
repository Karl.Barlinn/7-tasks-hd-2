import { valueBinder } from "../packages/binders.js";
import { comp } from "./comp.hd.js";

let system = hd.defaultConstraintSystem;

window.onload = () => {
  system.addComponent(comp);
  system.update();

  valueBinder(document.getElementById("celsius"), comp.vs.celsius);
  valueBinder(document.getElementById("fahrenheit"), comp.vs.fahrenheit);
};
