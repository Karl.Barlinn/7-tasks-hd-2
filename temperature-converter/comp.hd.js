export const comp = hd.component`
     var celsius, fahrenheit;

     constraint {
       (celsius -> fahrenheit) => celsius * (9/5) + 32;
       (fahrenheit -> celsius) => (fahrenheit - 32) * (5/9);
     }
`;
